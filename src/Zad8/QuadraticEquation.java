package Zad8;

public class QuadraticEquation {
   // Równanie kwadratowe:
    // ax^2 + bx + c = 0
//    private double a;
//    private double b;
//    private double c;
    private double a, b, c;

    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double calculateDelta() {
        return (b * b) - (4 * a * c);
    }

    public double calculateX1() {
        double delta = calculateDelta();
        if (delta == 0.0) {
            return (-b / (2 * a));
        } else if (delta > 0.0) {
//            return (-b - Math.sqrt(delta) / 2 * a);
            return (-b - Math.sqrt(delta)) / (2 * a);
        } else {
            System.out.println("No results.");
            return 0.0;
        }
    }

    public double calculateX2() {
        double delta = calculateDelta();
        if (delta == 0.0) {
            System.out.println("There is only one solution, it is X1");
            return calculateX1();
        } else if (delta > 0.0) {
//            return (-b + Math.sqrt(delta) / 2 * a);
            return (-b + Math.sqrt(delta)) / (2 * a);
        } else {
            System.out.println("No results.");
            return 0.0;
        }
    }
}



