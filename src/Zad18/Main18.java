package Zad18;
/*
Napisz klasę Person, Club, NoAdultException.
        a.  Klasa Person powinna zawierać, imię, nazwisko i wiek
        b.  Klasa Club powinna zawierać metodę enter(Person person),
        która wyrzuca wyjątek NoAdultException, jeżeli osoba jest niepełnoletnia.
 */
public class Main18 {
    public static void main(String[] args) {
        Club c = new Club();
        Person p1 = new Person(16);
        Person p2 = new Person(20);

        try {
            c.enter(p1);
        } catch (NoAdultException e) {
            e.printStackTrace();
        }
        //        throw new RuntimeException();
//        try {
//            throw new Exception();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }
}
