package Zad18;

public class Club{
        public boolean enter(Person osoba) throws NoAdultException{
            if (osoba.getAge() <= 18) {
                throw new NoAdultException();
            }
            System.out.println("Wchodzi");
            return true;
        }
}
