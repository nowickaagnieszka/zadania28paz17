package Zad4;

import java.util.Scanner;

public class Main4wyk2 {
    public static void scannerDzialania(){
        Calculator c = new Calculator();
        Scanner sc = new Scanner(System.in);
        boolean isWorking = true;
        while (isWorking) {
            String line = sc.nextLine();
            // getting line from scanner
            line = line.replace(" ", "");
//            line = line.replace("\\s+", "");
            // spowoduje usuniecie wszystkich spacji
//            line.toLowerCase() - wszystkie znaki są małe
//            if(line.equals("dodaj")) // Dodaj.toLowerCase == dodaj
            if(line.equals("quit")){
                isWorking = false;
                break;
            }
            // Przykład linia = '23 +      124'
            // linia po replace = '23+124'
            // indexOf+ == 2
            // slowa[0] 23
            // slowa[1] 124
            if (line.indexOf('+') != -1) { // znalazlem plusik
                // dzielimy linie na to co jest przed + i to co jest po +
                String[] slowa = line.split("[+]");
                // slowa[0] - przed +
                // slowa[1] - po +
                try {
                    int liczba1 = Integer.parseInt(slowa[0]); // zamiana slowa na liczbe
                    int liczba2 = Integer.parseInt(slowa[1]); // zamiana slowa na liczbe
                    int wynik = c.addTwoNumbers(liczba1, liczba2);
                    System.out.println("Wynik dodawania : " + wynik);
                }catch (NumberFormatException nfe){
                    System.err.println("Niepoprawny format liczbowy");
                }
            }
            // pozostałe przypadki
        }
    }

}
