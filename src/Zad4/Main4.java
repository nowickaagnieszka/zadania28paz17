package Zad4;

import java.util.Scanner;

/*
 Napisz program w którym jest jedna klasa 'Calculator'.
    Klasa reprezentuje model kalkulatora. Klasa powinna posiadać metodę:
        -   addTwoNumbers - która przyjmuje dwa parametry i zwraca wynik dodawania
        -   substractTwoNumbers - która przyjmuje dwa parametry i zwraca wynik odejmowania
        -   multiplyTwoNumbers - która przyjmuje dwa parametry i zwraca wynik mnożenia
        -   divideTwoNumbers - która przyjmuje dwa parametry i zwraca wynik dzielenia
    UWAGA! nie twórz pól klasy które reprezentują liczby działań. Podstawowym błędem popełnianym
    w tym zadaniu jest dodanie pól do klasy.
    Liczby na których masz wykonać działania są przekazywane jako argumenty/parametry metody!!!

    UWAGA! wynik jest zwracany z metody. W main'ie wykonaj działania na kalkulatorze,
    a następnie wartość zwróconą przypisz do zmiennej i wyświetl na konsoli.

    Wszystkie metody zwracają wartości. Stwórz maina, a w nim jedną instancję klasy Calculator,
    a następnie przetestuj działanie wszystkich metod.

    Dodatkowo Zad4*:
    Dodaj do kalkulatora powyżej obsługę komend. Użytkownik wpisuje komendy:
        +dodaj 20 30     i otrzymuje wynik dodawania.
        -odejmij 20 30     i otrzymuje wynik odejmowania
        *pomnóż 20 30     i otrzymuje wynik mnożenia
        /podziel 20 30     i otrzymuje wynik dzielenia

Zastosuj do tego pętlę while która ma trwać dopóki jest jakieś wejście:
while (scanner.hasNextLine())
 */
public class Main4 {
    public static void main(String[] args) {
    Calculator calculator = new Calculator();

    //a
//        int addResult = calculator.addTwoNumbers(20, 30);
//        System.out.println("Wynik dodawania: " + addResult);
//
//
//        int substractResult = calculator.substractTwoNumbers(20, 30);
//        System.out.println("Wynik odejmowania: " + substractResult);
//
//
//        int multiplyResult = calculator.multiplyTwoNumbers(20, 30);
//        System.out.println("Wynik mnożenia: " + multiplyResult);
//
//
//        int divideTwoNumbers = calculator.divideTwoNumbers(20, 30);
//        System.out.println("Wynik dzielenia: " + divideTwoNumbers);
//
//        int divideByZero = calculator.divideTwoNumbers(20, 0);
//        System.out.println("Wynik dzielenia: " + divideByZero);
    // b)
        Scanner scanner = new Scanner(System.in);

        String inputLine;
        do {
            System.out.println("Jest to prosty kalkulator, uzyj jednego ze znakow dzialania: +,-,*,/ i podaj dwie liczby");
            inputLine = scanner.nextLine(); // przyjmowanie komendy z wejscia

            String[] words = inputLine.split(" ");
            // command
            String command = words[0];

            if (command.equals("koniec")) {
                // wyskocz z pętli while
                break;
            }

            // numbers
            String firstNumber;
            String secondNumber;
            try {
                firstNumber = words[1];
                secondNumber = words[2];
            } catch (ArrayIndexOutOfBoundsException aioobe) {
                System.out.println("Nie wystarczajace parametry");
                continue;
            }

            int first;
            int second;

            try {
                first = Integer.parseInt(firstNumber);
                second = Integer.parseInt(secondNumber);
            } catch (NumberFormatException nfe) {
                System.err.println("Zly format liczby."); // err na czerwono
                // obsługa niepoprawnego wejścia
                continue;
            }

            if (command.equals("+")) {
                // dodawanie

                System.out.println(calculator.addTwoNumbers(first, second));
            } else if (command.equals("-")) {
                // odejmowanie

                System.out.println("Wynik: " + calculator.substractTwoNumbers(first, second));
            } else if (command.equals("*")) {
                // mnozenie

                System.out.println("Wynik: " + calculator.multiplyTwoNumbers(first, second));
            } else if (command.equals("/")) {
                // dodawanie

                System.out.println("Wynik: " + calculator.divideTwoNumbers(first, second));
            } else {
                System.out.println("Nieznana komenda.");
            }
        } while (!inputLine.equals("Koniec")); // wpisanie quit spowoduje wyjscie z kalkulatora/programu

    }
}




