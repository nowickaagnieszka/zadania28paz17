package Zad4;

import java.util.Scanner;

public class Main4wyk3 {
    public static void scannerSlowa() {
        Calculator c = new Calculator();
        Scanner sc = new Scanner(System.in);
        boolean isWorking = true;
        while (isWorking) {
            String line = sc.nextLine();
            // getting line from scanner
            line = line.toLowerCase();
//            line.toLowerCase() - wszystkie znaki są małe
//            if(line.equals("dodaj")) // Dodaj.toLowerCase == dodaj
            if (line.equals("quit")) {
                isWorking = false;
                break;
            }
            String[] slowa = line.split(" "); // slowa oddzielone spacjami
            // Przyklad linia = 'dodaj 1'
            // Przykład linia = 'Dodaj 123 12'
            // linia po tolowercase= 'dodaj 123 12'
            // slowa[0] dodaj
            // slowa[1] 123
            // slowa[2] 12
            if (slowa[0].equals("dodaj")) { // pierwszym slowem jest 'dodaj'
                try {
                    int liczba1 = Integer.parseInt(slowa[1]); // zamiana slowa na liczbe
                    int liczba2 = Integer.parseInt(slowa[2]); // zamiana slowa na liczbe
                    int wynik = c.addTwoNumbers(liczba1, liczba2);
                    System.out.println("Wynik dodawania : " + wynik);
                } catch (NumberFormatException nfe) {
                    System.err.println("Niepoprawny format liczbowy");
                } catch (ArrayIndexOutOfBoundsException aioobe) {
                    System.err.println("Niepoprawna ilość parametrów");
                }
            }
            // pozostałe przypadki
        }
    }

}
