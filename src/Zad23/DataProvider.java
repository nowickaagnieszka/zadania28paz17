package Zad23;
/*
jak jest interface to nie mozna dodawac metod i ich ciał, pola sa abstrakcyjne(puste),
ale jakby zrobic public abstract to juz by mozna bylo dodac metode;
 */
interface DataProvider {
  int nextInt();

  String nextString();

}
