package Zad23;

import java.util.Scanner;

public class  ConsoleDataProvider implements DataProvider {
    private String name = "Ktostam";
    private int wiek = 19;

    @Override
    public int nextInt() {
        System.out.println("Podaj wiek: ");
        Scanner in = new Scanner(System.in);
        return in.nextInt();
    }

    @Override
    public String nextString() {
        System.out.println("Podaj imie i nazwisko: ");
        Scanner in = new Scanner(System.in);
        return in.nextLine();
    }
}
