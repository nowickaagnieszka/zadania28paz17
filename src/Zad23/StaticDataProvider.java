package Zad23;

public class StaticDataProvider implements DataProvider {

    @Override
    public int nextInt() {
        return 25;
    }

    @Override
    public String nextString() {
        return "Jan Kowalski";
    }
}
