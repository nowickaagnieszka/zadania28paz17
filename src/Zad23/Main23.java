package Zad23;

import javax.xml.crypto.Data;
import java.util.Scanner;

/*
Utwórz klasę abstrakcyjną (interfejs**) DataProvider z dwiema metodami ( int nextInt(String name)
i String nextString(String name)) i jego trzy implementacje:
            ConsoleDataProvider
            StaticDataProvider
            RandomDataProvider

        Następnie utwórz program, który przy pomocy DataProvidera prosi o imię nazwisko i wiek, a następnie je wyświetla.
 */
public class Main23 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Jakiego providera uzyc: ");
        int i = in.nextInt();
        DataProvider dp = null;
        if( i==1){
            dp= new StaticDataProvider();
        } else if(i==2){
            dp = new ConsoleDataProvider();
        }  else if(i==3){
            dp = new RandomDataProvider();

        }

        if (dp !=null){
            int wiek = dp.nextInt();
            String name = dp.nextString();
            System.out.println("Wiek uzytkownika: "+ wiek);
            System.out.println("Imie i nazwisko: "+ name);
        }
    }
}
