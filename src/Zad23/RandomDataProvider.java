package Zad23;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomDataProvider implements DataProvider {
    private Random rand = new Random();
    private List<String> imiona = new ArrayList<>();

    public RandomDataProvider() {
        imiona.add("Mariusz Kowlaczyk");
        imiona.add("Dorota Nowak");
        imiona.add("Maria Kowal");
        imiona.add("Kasia Dzika");
    }


    @Override
    public int nextInt() {
        return rand.nextInt(100) + 1;
    }

    @Override
    public String nextString() {
        return imiona.get(rand.nextInt(imiona.size()));
    }
}