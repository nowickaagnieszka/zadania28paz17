package Zad27;

import java.util.*;

/*
        a. Napisz program tworzący mapę',' która przyporządkowuje każdej liczbie z zakresu od 1 do 30 informację o tym,
            czy liczba jest liczbą pierwszą.
        b. Wypisz wszystkie pary na ekran w pętli for (entrySet)
        c. Wypisz wartości zwrócone z mapy dla kluczy 1,4,5,7,9,12
        d. Napisz metodę, która zwraca wartość słowa. Użyj mapy, która przyporządkowuje każdej literze wartość.
        Domyślna wartość dla litery to 1, dla pozostałych znaków 0.

        Poniższe znaki mają określone wartości:
            y,h,q,x - 2
            ą,ę,ó,ż,ź,ć,ś - 3

        sygnatura metody:
        public static int rank(String text);
           Przykłady:
                    “ręka” -> 6
                    “   .;a” -> 1
 */
public class Main27 {
    public static void main(String[] args) {

        //a. Utwórz mapę dla liczb 1-30 z info czy jest liczba pierwszą
        //definiujemy nasza mapę
        System.out.println("a.");
        Map<Integer, String> isPrimaryMap = new HashMap<Integer, String>();

        //wypełniamy mape danymi
        for(int i=1; i<=30;i++){
            if(isPrimary(i)){
                isPrimaryMap.put(i,"is a primary number.");
            } else {
                isPrimaryMap.put(i,"isn't any primary number.");
            }
        }

        //b. wypisujemy po entrySet
        System.out.println("b.");
        for (Map.Entry<Integer, String> entry : isPrimaryMap.entrySet())
        {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        //c. Wypisz wartości dla kluczy 1,4,5,7,9,12
        System.out.println("c.");
        int[] keyList = new int[]{1,4,5,7,9,12};
        for (int key:keyList){
            System.out.println(key + "-" + isPrimaryMap.get(key));
        }
        
        //d. robimy mape z literami
        System.out.println("d.");
        String[] letters = new String[]{"a","ą","b","c","ć","d","e","ę","f","g","h","i","j","k","l","ł","m","n","ń","o","ó","p","q","r","s","ś","t","u","v","w","x","y","z","ź","ż"};
        Map<String, Integer> lettersValues = new HashMap<String, Integer>();
        String[] lettersFor2 = new String[]{"y","h","q","x"};
        String[] lettersFor3 = new String[]{"ą","ę","ó","ż","ź","ć","ś"};
        int value = 1;
        for(String letter:letters){
            value = 1;
            for(String lf2:lettersFor2){
                if(letter.equals(lf2)){
                    value = 2;
                    break;
                }
            }
            for(String lf3:lettersFor3){
                if(letter.equals(lf3)){
                    value = 3;
                    break;
                }
            }
            lettersValues.put(letter,value);
        }

        //prosimy uzytkownika o słowo
        System.out.println("Write a word for to get it's value: ");
        Scanner scr = new Scanner(System.in);
        String word = scr.nextLine();
        //rozbijamy na litery
        char[] lettersTemp = word.toCharArray();
        String[]lettersInWord = new String[lettersTemp.length];
        //zamieniamy litery na stringi
        for(int i=0;i<lettersTemp.length;i++){
            lettersInWord[i] = String.valueOf(lettersTemp[i]);
        }
        //liczymy wartośc słowa
        int valueOfTheWord = 0;
        for(String sign : lettersInWord){
            //System.out.println(sign + "-" + lettersValues.get(sign));
            if(lettersValues.get(sign) != null)
                valueOfTheWord += lettersValues.get(sign);
        }
        System.out.println("Total word '" + word + "' value is "+valueOfTheWord+".");

    }

    /*******************************************************
     Funkcja sprawdza czy liczba jest pierwsza, czyli cyz nie dzieli się bez reszty
     przez nic oprócz samej siebie i 1. Sprawdzana liczba musi byc dodatnia.
    ********************************************************/
    public static boolean isPrimary(int number){
        boolean isPrim = true;
        if(number<=0){ //nie mozemy sprawdzać licz ujemnych i zera, więc wychodzimy
            return false;
        }

        for(int i = 2; i<number; i++) {
            if(number%i==0){
                isPrim = false;
                break;
            }
        }
        return isPrim;
    }
}
