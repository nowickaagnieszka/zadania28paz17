package Zad10;
/*
Stwórz program, w programie stwórz klasę "Field" które reprezentuje planszę (pole) podobne do takiego
    jak gra w statki. Pole powinno posiadać tablicę dwuwymiarową KWADRATOWĄ zmiennych typu boolean.
    Każde pole posiada wartość true/false jeśli było/nie było sprawdzane.
    Wielkość tego pola powinna być określana w konstruktorze.

    dodaj metodę:
        - printField - która wypisuje na konsolę kwadrat i wartości pola.
            X - jeśli komórka była sprawdzana
            O - jeśli komórka nie była sprawdzana
        - checkCell - która jako parametrY przyjmuje współrzędną
            X oraz współrzędną Y komórki do sprawdzenia.
            Jeśli komórka nie była jeszcze sprawdzana to ją sprawdza,
            jeśli komórka była sprawdzana, to wypisuje komunikat że "komórka była sprawdzana".
        - w klasie Main5wyk2 w metodzie main dodaj instancję klasy "Field" a następnie dodaj obsługę
            komend ze Scanner'a, która pozwoli wywoływać metody printField oraz checkCell.
 */
public class Main10 {
    public static void main(String[] args) {
        Field field = new Field(5);
        field.print();
        field.check(1,1);
        field.check(1,2);
        field.check(2,4);
        field.check(2,4);
        field.print();
    }

}
