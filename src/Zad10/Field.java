package Zad10;
/*

 */
public class Field {
    private boolean[][] pole;

    public Field(int size) {
        this.pole = new boolean[size][size];
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                pole[i][j]=false;
            }
        }
    }

    public void print(){
        for(int i = 0; i < pole.length; i++){
            for(int j = 0; j < pole.length; j++){
                if(pole[i][j]) System.out.printf("X ");
                else System.out.printf("O ");
            }
            System.out.println();
        }
    }

    public void check(int i, int j){
        if(!pole[i][j]) pole[i][j]=true;
        else System.out.println("alredy checked");
    }

}
