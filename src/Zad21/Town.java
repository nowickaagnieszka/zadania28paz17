package Zad21;


import java.util.ArrayList;
import java.util.List;

public class Town {
    public List<Citizen> obywatele = new ArrayList<>();

    public Town() {
    }

    public void addCitizen(Citizen c) {
        obywatele.add(c);
    }

    public int howManyCanVote() {
        int counter = 0;
        // zliczamy ilosc obywateli ktorzy moga glosowac
        for (Citizen obywatel : obywatele) {
            if (obywatel.canVote()) {
                counter++;
            }
        }
        return counter;
    }

    public List<Citizen> whoCanVote() {
        List<Citizen> whoCanVote = new ArrayList<>();
        // do nowej listy dodaje tylko tych obywateli ktorzy moga glosowac
        for (Citizen obywatel : obywatele) {
            if (obywatel.canVote()) {
                whoCanVote.add(obywatel);
            }
        }

        // zwracam liste
        return whoCanVote;
    }

    public void whoCanVote2() {
        // do nowej listy dodaje tylko tych obywateli ktorzy moga glosowac
        for (Citizen obywatel : obywatele) {
            if (obywatel.canVote()) {
                System.out.println(obywatel);
            }
        }
    }
}
