package Zad21;

public abstract class Citizen {
    protected String name;

    public Citizen(String name) {
        this.name = name;
    }

    public String getName() {
        return name ;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract boolean canVote();

    @Override
    public String toString() {
        return getName() + " " + getClass();
    }
}
