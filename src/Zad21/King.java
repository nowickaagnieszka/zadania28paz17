package Zad21;

public class King extends Citizen {

    public King(String name) {
        super(name);
    }

    @Override
//    public boolean canVote() {
//        return false;
//    }
    public boolean canVote() {
        return false;
    }

    @Override
    public String getName() {
        return "Król: " + name;
    }
}
