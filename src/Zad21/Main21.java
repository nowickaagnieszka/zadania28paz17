package Zad21;
/*
Stwórz klasę Citizen oraz klasy dziedziczące
            Peasant(Chłop),
            Townsman(Mieszczanin),
            King(Król),
            Soldier(Żołnierz)
        Wszystkie klasy posiadają pole imie
        Citizen powinien być klasą abstrakcyjną która posiada metodę abstrakcyjną 'canVote' która zwraca true dla
            townsman'a i soldier'a, ale false dla chłopa i króla.
        Stwórz klasę Town która posiada listę Citizenów. Dodaj do niej kilku citizenów (różnych w mainie) i
        stwórz metody howManyCanVote które zwracają ilość osób które mogą głosować.
        Stwórz w klasie Town metodę "whoCanVote" która zwraca imiona osób które mogą głosować.
 */

public class Main21 {
    public static void main(String[] args) {
        Town miato = new Town();
        miato.addCitizen(new King("krol"));
        miato.addCitizen(new Townsman("townsman1"));
        miato.addCitizen(new Townsman("townsman2"));
        miato.addCitizen(new Townsman("townsman3"));
        miato.addCitizen(new Townsman("townsman4"));
        miato.addCitizen(new Townsman("townsman5"));
        miato.addCitizen(new Townsman("townsman6"));
        miato.addCitizen(new Townsman("townsman7"));
        miato.addCitizen(new Soldier("sol2"));
        miato.addCitizen(new Soldier("sol1"));
        miato.addCitizen(new Peasant("Peasant2"));
        miato.addCitizen(new Peasant("Peasant1"));

        System.out.println("Ilosc obywateli mogacych glosowac:" + miato.howManyCanVote());
        System.out.println("Kto moze glosowac: " + miato.whoCanVote());
        miato.whoCanVote2();
//        System.out.println("Kto moze glosowac: " + miato.whoCanVote2());
    }
}
