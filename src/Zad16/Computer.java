package Zad16;

public class Computer {
    private String name;
    private double cpuPower;
    private int driveCapacity;
    private int ramCapacity;
    private String gpuName;

    public Computer(String name, double cpuPower, int driveCapacity, int ramCapacity) {
        this.name = name;
        this.cpuPower = cpuPower;
        this.driveCapacity = driveCapacity;
        this.ramCapacity = ramCapacity;
    }

    public boolean isForGames() {
        if (getProcessingPower() > 10) {
            return true;
        }
        return false;
    }

    public double getProcessingPower() {
        if (hasGPU()) {
            return (cpuPower * 1.45 + (0.25 * ramCapacity));
        }
        return (cpuPower * 0.95 + (0.3 * ramCapacity));
    }

    public boolean hasGPU() {
        return gpuName != null; // true jesli jest
    }

    public void setGPUName(String gpuName) {
        this.gpuName = gpuName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCpuPower() {
        return cpuPower;
    }

    public void setCpuPower(double cpuPower) {
        this.cpuPower = cpuPower;
    }

    public int getDriveCapacity() {
        return driveCapacity;
    }

    public void setDriveCapacity(int driveCapacity) {
        this.driveCapacity = driveCapacity;
    }

    public int getRamCapacity() {
        return ramCapacity;
    }

    public void setRamCapacity(int ramCapacity) {
        this.ramCapacity = ramCapacity;
    }

}
