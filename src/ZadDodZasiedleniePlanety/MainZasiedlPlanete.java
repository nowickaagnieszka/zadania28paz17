package ZadDodZasiedleniePlanety;
/*
Ludzie postanowili zasiedlić nową planetę podobną do Ziemi - Nadziejus Prymitywus.
Planeta ta jest oddalona od Ziemi o dokładnie 93 lata. W tym czasie ludzie, którzy mają skolonizować planetę
będą pogrążeni w hibernacji.  Przez ten okres pilnować ich będzie załoga statku dokładnie 29 osób.
Pilnować będzie zawsze jedna osoba z załogi, reszta będzie korzystać z hibernacji.
Na podstawie różnych zmiennych oblicz jak często każdy z nich musi być wybudzony,
by  każdy stracił z życia po równym okresie (z uwzględnieniem godzin):
a) przypadek idealnego równego podziału uwzględniający lata przestępne
b) przypadek w którym na lata podróży 47-53 przypada podróż przez magiczny pas Frajezautoura i
należy wziąć pod uwagę, że każdy z załogi powinien móc zobaczyć ten efekt.
c)przypadek wystąpienia trzech awarii zmuszający obudzenia dodatkowych trzech osób
d) niezdolność do wykonywania obowiązków służbowych dwóch osób
e) wykonaj obliczenia uwzględniając przypadki od b do d.
 */
public class MainZasiedlPlanete {
    public static void main(String[] args) {

    }
}
