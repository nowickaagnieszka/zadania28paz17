package Zad5wyk2;

public class Main5wyk2 {
    public static void main(String[] args) {
        Square square = new Square(20.0);
        System.out.println("Area: " + square.calculateArea());
        System.out.println("Perimeter: " + square.calculatePerimeter());

        Rectangle rectangle = new Rectangle(20.0, 30.0);
        System.out.println("Area: " + rectangle.calculateArea());
        System.out.println("Perimeter: " + rectangle.calculatePerimeter());

        Rectangle.calculatePerimetera();

        Circle circle = new Circle(20.0);
        System.out.println("Area: " + circle.calculateArea());
        System.out.println("Perimeter: " + circle.calculatePerimeter());
    }
}



