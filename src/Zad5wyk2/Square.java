package Zad5wyk2;

public class Square {
    private double a;

    public Square(double a) {
        this.a = a;
    }

    public double calculatePerimeter() {
        return 4 * a;
    }

    public double calculateArea() {
        return a * a;
    }

}
