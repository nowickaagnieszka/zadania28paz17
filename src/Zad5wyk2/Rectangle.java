package Zad5wyk2;

public class Rectangle {
    public static final double REC_PI = 3.14;
    private double a, b;

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public static double calculatePerimetera() {
        return 0.0;
    }

    public double calculatePerimeter() {
        return (2 * a) + (2 * b);
    }

    public double calculateArea() {
        return a * b;
    }

}
