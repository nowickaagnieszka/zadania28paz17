package Zad12;
/*
 Stwórz program, w programie stwórz klasę Circle, Square i Rectangle (lub skopiuj/użyj klas z zad5).
    Stwórz maina, a w nim obsługę komend:
        - oblicz [pole/obwód] [kwadrat/koło/prostokat] X Y

    gdzie:
        pole/obwód to opcja. Możemy wpisać jedno z dwóch.
        kwadrat/koło/prostokat to opcja. Możemy wpisać jedno z trzech.
        X oraz Y to długości boków/promienia. W przypadku koła podajemy tylko X jako długość promienia,
        w przypadku prostokątu podajemy X oraz Y, a w przypadku kwadratu również podajemy X.
 */
public class Main12 {
    public static void main(String[] args) {
        Square square = new Square(3.0);
        System.out.println("Pole kwadratu: " + square.obliczPole());
        System.out.println("Obwod kwadratu: " + square.obliczObwod());
        System.out.println();

        Rectangle rectangle = new Rectangle(2.0, 3.0);
        System.out.println("Pole prostokata: " + rectangle.obliczPole());
        System.out.println("Obwod prostokata: " + rectangle.obliczObwod());
        System.out.println();

        Circle circle = new Circle(3.0);
        System.out.println("Pole kola: " + circle.obliczPole());
        System.out.println("Obwod kola: " + circle.obliczObwod());
    }
}


