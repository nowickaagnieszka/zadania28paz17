package Zad12;

public class Circle {
    private double X;

    public Circle(double X) {
        this.X = X;
    }
    public double obliczPole(){
        return (Math.PI* (X*X));
    }
    public double obliczObwod(){
        return 2* (Math.PI*X);
    }
    @Override
    public String toString() {
        return super.toString();
    }
}
