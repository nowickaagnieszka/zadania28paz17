package Zad12;

import java.time.Year;

public class Square {
    private double X;

    public Square(double X) {
        this.X = X;
    }
    public double obliczPole(){
        return X*X;
    }
    public double obliczObwod(){
        return 4*X;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
