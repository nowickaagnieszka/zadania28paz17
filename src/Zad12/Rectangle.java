package Zad12;

public class Rectangle {
    private double X;
    private double Y;

    public Rectangle(double X, double Y) {
        this.X = X;
        this.Y = Y;
    }
    public double obliczPole(){
        return X*Y;
    }
    public double obliczObwod(){
        return(2*X)+(2*Y);
    }

//    public double getX() {
//        return X;
//    }
//
//    public void setX(double x) {
//        X = x;
//    }
//
//    public double getY() {
//        return Y;
//    }
//
//    public void setY(double y) {
//        Y = y;
//    }
//
//    @Override
//    public String toString() {
//        return "Rectangle{" +
//                "X=" + X +
//                ", Y=" + Y +
//                '}';
//    }
}
