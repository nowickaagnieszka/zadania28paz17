package Zad9;

public class Car {
    private String CarName;
    private int maxNoSeats;
    private int pojSilnika;
    private int powerKM;
    private int currentSpeed;
    private int currentPrzebieg;
    private int rokProcukcji;
    private int passengerNo;

    public int getCurrentSpeed() {
        return currentSpeed;
    }

    public String getCarName() {
        return CarName + " (" + rokProcukcji + ")";
    }
    public void addPassenger(){
        if(passengerNo<maxNoSeats){
            passengerNo = passengerNo+1;
        }
    }
    public void removePassenger(){
        if(passengerNo>0){
            passengerNo = passengerNo-1;
        }
    }
    public void speedIncrease(){
        int maxSpeed = (int)(1.2*powerKM);
        if(powerKM>200){
            maxSpeed=powerKM;
        }
        if(passengerNo>0 && currentSpeed<maxSpeed){
            currentSpeed = currentSpeed+1;
        }
    }
    public void speedDecrease(){
        if(passengerNo>0 && currentSpeed>0){
            currentSpeed = currentSpeed-1;
        }
    }
    public Car(String carName, int maxNoSeats, int pojSilnika, int powerKM, int currentPrzebieg, int rokProcukcji) {
        CarName = carName;
        this.maxNoSeats = maxNoSeats;
        this.pojSilnika = pojSilnika;
        this.powerKM = powerKM;
        this.currentSpeed = 0;
        this.currentPrzebieg = currentPrzebieg;
        this.rokProcukcji = rokProcukcji;
        this.passengerNo = 0;

        }


}
