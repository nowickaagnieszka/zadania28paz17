package Zad9;
/*
Do stworzenia jest aplikacja, w której stwórz klasę "Car" reprezentującą samochód.
    a) Samochód ma pola:
        - nazwę/markę,
        - ilość siedzeń,
        - pojemność silnika,
        - ilość koni mechanicznych,
        - obecną prędkość,
        - obecny przebieg,
        - rok produkcji

    Samochód ma metody:
    b)
        - getCarName - która ZWRACA nazwę/markę samochodu oraz obecny rok produkcji
    c)
        - addPassenger - która dodaje pasażera
        - removePassenger - która odejmuje pasażera
    d)
        - speedIncrease - która zwięsza obecną prędkość samochodu
        - speedDecrease - która zmniejsza prędkość obecną samochodu
        (samochód może przyspieszać lub zwalniać tylko jeśli jest minimum 1 pasażer[zakładamy że to kierowca])

    e) Zmodyfikuj samochód tak, aby mógł osiągać max prędkość nie wyższą niż (1.2*ilość koni mech),
       jeśli ma do 200 koni mech oraz nie wyższą niż (1.0*ilość koni mechanicznych) jeśli ilość koni przekracza 200.
 */
public class Main9 {
    public static void main(String[] args) {
        Car car = new Car("BMW",5, 1995,265, 197000, 2005);
        car.addPassenger();
        for (int i = 0; i <300 ; i++) {
            car.speedIncrease();
        }
        car.removePassenger();
        System.out.println("Samochod jedzie predkoscia: " +car.getCurrentSpeed()+ " km/h.");
        }

}
