package Zad28;

public class Alfabet {
    public String[] alfabet = {"a","ą","b","c","ć","d","e","ę","f","g","h","i","j","k","l","ł","m","n","ń","o","ó","p","q","r",
            "s","ś","t","u","w","x","y","z","ź","ż"};

    public Alfabet(){
    }
    //c. Napisz metodę, która przyjmuje argument typu String i zwraca informację o tym,
    //   czy zawiera on jedynie znaki należące do polskiego alfabetu.
    public boolean sprawdzenie(String wyrazenie){
        char[] zbior1 = wyrazenie.toLowerCase().toCharArray();
        String[] zbior = new String[zbior1.length];
        for (char i = 0; i <wyrazenie.length() ; i++) {
            zbior[i] = String.valueOf(zbior1[i]);
        }

        boolean znalezione = false;
        for (String litera : zbior) {
            znalezione = false;
            for (String znak : this.alfabet) {
                if (znak.equals(litera)) {

                    znalezione = true;
                    break;
                }
            }
            if (znalezione == false) {
                System.out.println("Nie znaleziono " + litera + " w alfabecie");
                return false;
            }
        }
        return true;
    }
}
