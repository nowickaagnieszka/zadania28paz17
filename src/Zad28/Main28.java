package Zad28;
/*
Napisz program zawierający zbiór wszystkich liter występujących w języku polskim.
    a. Wypisz wszystkie elementy zbioru na ekran.
    b. Sprawdź, czy w zbiorze istnieją elementy a,ą,x,z,ź,ż
    c. Napisz metodę, która przyjmuje argument typu String i zwraca informację o tym,
    czy zawiera on jedynie znaki należące do polskiego alfabetu.
 */
public class Main28 {
    public static void main(String[] args) {
        Alfabet polski = new Alfabet();
        //a. Wypisz wszystkie elementy zbioru na ekran
        //  for (int i = 0; i <alfabet.length ; i++) {
        // System.out.print(alfabet[i]+", ");
        for (String znak : polski.alfabet) {
            System.out.println(znak + ", ");
        }
        System.out.print("\b\b\n");
        //b. Sprawdź, czy w zbiorze istnieją elementy a,ą,x,z,ź,ż
        String[] zbior = {"a", "ą", "x", "Z", "ź", "ż"};
        boolean znalezione = false;
        for (String litera : zbior) {
            znalezione = false;
            for (String znak : polski.alfabet) {
                if (znak.equals(litera)) {
                    System.out.println("Znaleziono " + litera);
                    znalezione = true;
                    break;
                }
            }
            if (znalezione == false) {
                System.out.println("Nie znaleziono " + litera + " w alfabecie");
            }
        }
    boolean test = polski.sprawdzenie("Dupa");
        System.out.println(test);
    }
}
