package Zad13;

import java.util.ArrayList;

public class Student {
    private String name;
    private String nrIndeksu;
    private ArrayList<Integer> listaOcen = new ArrayList<>(); // opcja 2

    public Student(String name, String nrIndeksu) {
        this.name = name;
        this.nrIndeksu = nrIndeksu;
//        this.listaOcen = new ArrayList<>(); // opcja 1
    }

    public Student() {
        //        this.listaOcen = new ArrayList<>(); // opcja 1
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNrIndeksu() {
        return nrIndeksu;
    }

    public void setNrIndeksu(String nrIndeksu) {
        this.nrIndeksu = nrIndeksu;
    }

    public ArrayList<Integer> getListaOcen() {
        return listaOcen;
    }

    public void setListaOcen(ArrayList<Integer> listaOcen) {
        this.listaOcen = listaOcen;
    }

    public double calculateMean() {
        double sum = 0.0;
        for (Integer grade : listaOcen) {
            System.out.println("Dodaje: " + grade);
            sum += grade;
        }
        return sum / listaOcen.size();
    }
/// 5 3 4 2 1


    public boolean didIPass() {
        for (Integer grade : listaOcen) {
            if (grade <= 2) {
                System.out.println("Ocena:  "+ grade + " koncze petle");
                return false;
            }
        }
        System.out.println("Brak ocen niedostatecznych, zdalem!");
        return true;
    }

    // 5 3 4 2 1
    public int countLowGrades() {
        int counter = 0;
        for (Integer grade : listaOcen) {
            if (grade <= 2) {
                System.out.println("Ocena:  "+ grade + " zwiekszam licznik");
                counter++;
            }else{
                System.out.println("Nie zwiekszam licznika");
            }
        }
        System.out.println("Koncze zliczanie ocen");
        return counter;
    }

    public void addGrade(int grade) {
        listaOcen.add(grade);
    }
}
