package Zad20;

public abstract class Figure {
    protected double field;

    public abstract double calculateArea();
    public abstract double calculatePerim();

    public void printSomething(){
        System.out.println("Something");
    }

}
