package Zad30;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

/*
Stwórz aplikację która na wciśnięcie klawisza "Enter" wypisuje obecną datę i godzinę w (wybranym przez Ciebie) formacie.
Jeśli użytkownik wpisze 'quit' to zakoncz program.
 */
public class Main30 {
    public static void main(String[] args) {
        Scanner scr = new Scanner(System.in);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm:ss yyyy-MM-dd");
        System.out.println("Press [Enter]");
        while(scr.nextLine().isEmpty()){

            System.out.println(LocalDateTime.now().format(formatter));
        }

    }
}
