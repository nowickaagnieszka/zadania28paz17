package Gra;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class GraMilionerzy implements Serializable {
    public class Pytanie implements Serializable{
        int id;
        String tresc;

        public Pytanie(int id, String tresc) {
            this.id = id;
            this.tresc = tresc;
        }

        @Override
        public String toString() {
            return this.tresc;
        }
    }

    /**
     * @param cos
     */
    public class Odp implements Serializable{
        int id;
        String odpA;
        String odpB;
        String odpC;
        String odpD;
        String poprawnaOdp;

        public Odp(int id, String odpA, String odpB, String odpC, String odpD, String poprawnaOdp) {
            this.id = id;
            this.odpA = odpA;
            this.odpB = odpB;
            this.odpC = odpC;
            this.odpD = odpD;
            this.poprawnaOdp = poprawnaOdp;

        }

        @Override
        public String toString() {
            return "Możliwe odpowiedzi:\nA: " + this.odpA + "\nB: " + this.odpB + "\nC: " + this.odpC + "\nD: " + this.odpD;
        }
    }

    public boolean zapisPytanDoPliku(){
        Pytanie pyt;
        Odp odp;
        ObjectOutputStream strumienOut;


        List<Pytanie> pytania = new ArrayList<>();
        List<Odp> odpowiedzi = new ArrayList<>();


        pyt = new Pytanie(1, "Jaka jest stolica Polski?");
        odp = new Odp(1, "Gdynia", "Cypr", "Warszawa", "Szczecin", "C");
        pytania.add(pyt);
        odpowiedzi.add(odp);

        pyt = new Pytanie(2, "Jaka w październiku jest kalendarzowa pora roku?");
        odp = new Odp(2, "Wiosna", "Lato", "Jesien", "Zima", "C");
        pytania.add(pyt);
        odpowiedzi.add(odp);

        pyt = new Pytanie(3, "Jaka znana z bajek para zgubiła się w lesie?");
        odp = new Odp(3, "Jas i Malgosia", "Stas i Nela", "Bolek i Lolek", "Flip i Flap", "A");
        pytania.add(pyt);
        odpowiedzi.add(odp);

        pyt = new Pytanie(4, "Jaką kategoria okresla się prawo jazdy na motocykl?");
        odp = new Odp(4, "A", "B", "C", "D", "A");
        pytania.add(pyt);
        odpowiedzi.add(odp);

        pyt = new Pytanie(5, "Kto napisał \'Pana Tadeusza\'?");
        odp = new Odp(5, "A.Mickiewicz", "B.Prus", "C.Miłosz", "D.Stenka", "A");
        pytania.add(pyt);
        odpowiedzi.add(odp);

        pyt = new Pytanie(6, "Jaki taniec zwyczajowo tańczony jest na Studniówce?");
        odp = new Odp(6, "Breakdance", "Walc wiedeński", "Kaczuchy", "Polonez", "D");
        pytania.add(pyt);
        odpowiedzi.add(odp);

        pyt = new Pytanie(7, "Jakiego języka programowania uczysz sie na tym oto kursie?");
        odp = new Odp(7, "C++", "angielskiego", "Whitespace", "Java", "D");
        pytania.add(pyt);
        odpowiedzi.add(odp);

        pyt = new Pytanie(8, "Jaka marka samochodów zwyczajowo uznawana jest za najlepszą na świecie?");
        odp = new Odp(8, "Bugatti", "Audi", "Rolls-Royce", "VW Passat TDI", "C");
        pytania.add(pyt);
        odpowiedzi.add(odp);

        pyt = new Pytanie(9, "Kto był załozycielem portalu Facebook?");
        odp = new Odp(9, "Bill Gates", "Steve Jobs", "Mark Zuckerberg", "James Bond", "C");
        pytania.add(pyt);
        odpowiedzi.add(odp);

        pyt = new Pytanie(10, "Kim był w przeszłosci Nikki Lauda?");
        odp = new Odp(10, "Kierowcą rajdowym", "Kierowcą F1", "Miliarderem", "Zwycięzcą w grze Millionerzy", "B");
        pytania.add(pyt);
        odpowiedzi.add(odp);


        try {
            strumienOut = new ObjectOutputStream(new FileOutputStream("c:\\roboczy\\pytaniaDoGry.txt"));
            for (Object temp:pytania){
                pyt = (Pytanie)temp;
                strumienOut.writeObject(pyt);

            }
            strumienOut.close();


            strumienOut = new ObjectOutputStream(new FileOutputStream("c:\\roboczy\\odpDoGry.txt"));
            for (Object temp:odpowiedzi){
                odp = (Odp)temp;
                strumienOut.writeObject(odp);
            }
            strumienOut.close();
        } catch (IOException e){
            System.out.println("Błąd zapisu do pliku."+ e.getMessage()+"\n"+e.getStackTrace());
            return false;
        }
        return true;
    }

    public void run() throws ClassNotFoundException {
        Pytanie pyt;
        Odp odp;
        Random losowanie;
        int los;
        Scanner odczyt = new Scanner(System.in);
        String odpowiedzUzytkownika;
        ObjectInputStream inputStream;

        List<Pytanie> pytania;
        List<Odp> odpowiedzi;
        List<Integer> wykorzystaneID;

        pytania = new ArrayList<>();
        odpowiedzi = new ArrayList<>();
        wykorzystaneID = new ArrayList<>();

        try {
            inputStream = new ObjectInputStream(new FileInputStream("c:\\roboczy\\pytaniaDoGry.txt"));
            while (true) {
                try {
                    pyt = (Pytanie) inputStream.readObject();
                    pytania.add(pyt);
                } catch (EOFException e) {
                    inputStream.close();
                    break;
                }
            }

            inputStream = new ObjectInputStream(new FileInputStream("c:\\roboczy\\odpDoGry.txt"));
            while (true) {
                try {
                    odp = (Odp) inputStream.readObject();
                    odpowiedzi.add(odp);
                } catch (EOFException e) {
                    inputStream.close();
                    break;
                }
            }

        } catch (IOException e)
        {
            System.out.println("Błąd odczytu z pliku." + e.getMessage());
            zapisPytanDoPliku();
            System.out.println("Pytania zostały zapisane w nowym pliku. Prosze uruchomic program raz jeszcze.");
        }
        losowanie = new Random();
        boolean znalezionoID;

        while (true) {
            do {
                znalezionoID = false;
                los = losowanie.nextInt(pytania.size());
                for (Object ID : wykorzystaneID) {
                    if (los == Integer.parseInt(ID.toString())) {
                        znalezionoID = true;
                        break;
                    }
                }
                if (wykorzystaneID.size() == pytania.size()) {
                    System.out.println("Nie mam wiecej pytań, prosimy przesłać login i hasło do konta bankowego na adres hakier@okradamy.pl, a obiecujemy, że przelejemy Ci million złotych.\nKONIEC GRY");
                    return;
                }
            } while (znalezionoID);

            wykorzystaneID.add(los);
            pyt = (Pytanie) pytania.get(los);
            System.out.println(pyt.toString());
            odp = null;

            for (Object odpZlisty : odpowiedzi) {
                odp = (Odp) odpZlisty;
                if (pyt.id == odp.id) {
                    break;
                }

            }

            System.out.println(odp.toString());
            boolean niepoprawnaOdpUzytk;
            do {
                niepoprawnaOdpUzytk = false;
                System.out.println("Wskaz która odp jest prawidlowa: ");
                odpowiedzUzytkownika = odczyt.nextLine();
                odpowiedzUzytkownika = odpowiedzUzytkownika.toUpperCase();
                if (!odpowiedzUzytkownika.equals("A")
                        && !odpowiedzUzytkownika.equals("B")
                        && !odpowiedzUzytkownika.equals("C")
                        && !odpowiedzUzytkownika.equals("D")) {
                    niepoprawnaOdpUzytk = true;
                    System.out.println("Wprowadz jedna z dozwolonych odp A-D.");
                }

            } while (niepoprawnaOdpUzytk);

            if (odpowiedzUzytkownika.equals(odp.poprawnaOdp)) {
                System.out.println("Gratuluje");
            } else {
                System.out.println("Mylisz sie, bo prawidlowa odp to: " + odp.poprawnaOdp + "\nPrzegrałes!");
                break;
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("Witamy w grze!!!");
        GraMilionerzy gra = new GraMilionerzy();
        try {
            gra.run();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
