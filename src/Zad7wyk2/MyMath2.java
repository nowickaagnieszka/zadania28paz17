package Zad7wyk2;

public class MyMath2 {
    public int abs(int a) {
        if (a < 0) {
            a = -a;
        }
        return a;
    }

    public double abs(double a) {
        if (a < 0) {
            a = -a;
        }
        return a;
    }

    public double pow(int a, int b) {
        if (b == 0) {
            return 1;
        }else if(b < 0){
            throw new IllegalArgumentException();
        }
        int t = a;
        for (int i = 0; i < b; i++) {
            t *= a;
        }
        return a;
    }

    public double pow(double a, int b) {
        if (b == 0) {
            return 1;
        }else if(b < 0){
            throw new IllegalArgumentException();
        }
        double t = a;
        for (int i = 0; i < b; i++) {
            t *= a;
        }
        return a;
    }
}
