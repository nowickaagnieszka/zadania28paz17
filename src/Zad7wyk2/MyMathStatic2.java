package Zad7wyk2;

public class MyMathStatic2 {
    public static int abs(int a) {
        if (a < 0) {
            a = -a;
        }
        return a;
    }

    public static double abs(double a) {
        if (a < 0) {
            a = -a;
        }
        return a;
    }

    public static double pow(int a, int b) {
        if (b == 0) {
            return 1;
        }else if(b < 0){
            throw new IllegalArgumentException();
        }
        int t = a;
        for (int i = 0; i < b; i++) {
            t *= a;
        }
        return a;
    }

    public static double pow(double a, int b) {
        if (b == 0) {
            return 1;
        }else if(b < 0){
            throw new IllegalArgumentException();
        }
        double t = a;
        for (int i = 0; i < b; i++) {
            t *= a;
        }
        return a;
    }
}
