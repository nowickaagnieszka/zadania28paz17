package Zad7wyk2;

import Zad7.MyMath;

public class Main7wyk2 {
    public static void main(String[] args) {
        // obiektowo
        MyMath2 math = new MyMath2();

        // odwołania w kontekście obiektowym
        // musi byc obiekt na ktorym ja moge wywolac metode
        System.out.println(math.abs(1));
        System.out.println(math.abs(1.0));

        // statycznie
        // wywolania z kontekstu statycznego
        // nie potrzebujemy obiektu
        // piszemy: NAZWA_KLASY.NAZWA_METODY(PARAMETRY);
        System.out.println(MyMathStatic2.abs(1));
        System.out.println(MyMathStatic2.abs(1.0));
    }
}

