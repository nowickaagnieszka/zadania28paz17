package Zad29;

import java.text.ParseException;

public class WrongDateTimeException extends RuntimeException {

        // Parameterless Constructor
        public WrongDateTimeException() {}

        // Constructor that accepts a message
        public WrongDateTimeException(String message)
        {
            super("Wrong DateTime Format! "+message);
        }

}
