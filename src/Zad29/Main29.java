package Zad29;
/* (Time): (+rozrozniaj formaty po yyy itd.)
 Stwórz aplikacje która ze scannera przyjmuje datę w formacie '2013-03-20 12:40' (ROK-MSC-DZIEN GODZINA:DATA)
 i wypisuje komunikat ze data jest w poprawnym formacie lub
 (jeśli nie jest) rzuca Exception "WrongDateTimeException" który jest wyjątkiem,
  który dziedziczy po RuntimeException (napisz go sam/a).

  Dodatkowo.** Zmodyfikuj aplikację lub utwórz podobną aplikację, która na początku prosi o format daty (np. yyyy-mm-dd HH:mm)
  a po podaniu formatu daty działa tak jak poprzednia aplikacja,
  czyli przyjmuje datę w podanym formacie (tym wpisanym na początku aplikacji).
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Main29 {
    public static void main(String[] args) {
        System.out.println("Podaj date w formacie yyyy-MM-dd hh:mm");
        Scanner scr = new Scanner(System.in);
        String dataStr = scr.nextLine();

        try{

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");

            Date date = dateFormat.parse(dataStr);


            System.out.println("Udało się - wyprowadzona data jest w poprawnym formacie!\n"+date);

        } catch (ParseException ex) {
            System.out.println("Error Parsing Date: "+ex.getMessage());
            throw new WrongDateTimeException("Olaboga, co to za data.....");
        }

    }
}
