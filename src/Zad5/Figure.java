package Zad5;

public interface Figure {
    public double obliczPole();
    public double obliczObwod();
}
