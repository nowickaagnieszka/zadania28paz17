package Zad5;
/*
Stwórz program, a w nim:
a) klasę kwadrat która jako parametr konstruktora przyjmuje długość krawędzi.
W klasie stwórz metodę "obliczObwod" i "obliczPole" ktore ZWRACA wartości. Stwórz maina i przetestuj działanie.
b) klasę prostokąt która przyjmuje w konstruktorze dwa parametry (długość obu boków) i stwórz  metodę "obliczObwod" i "obliczPole",
  ktora ZWRACA wartości pola i obwodu bryły. Stwórz maina i przetestuj działanie.
c) klasę koło, która jako parametr przyjmuje promień. Dodaj w klasie koło metodę "obliczObwod" i "obliczPole",
 ktore ZWRACA wartości. Stwórz maina i przetestuj działanie.

+ odpowiedz na pytanie: czym jest polimorfizm metod i jak tutaj działa?
Nadpisanie @overridem, albo w metodie mozna zmienic ilosc parametrow lub ich typ i te roznice w (){ parametrow to polimorfizm}
 */
import java.util.ArrayList;
import java.util.List;

public class Main5 {
    public static void main(String[] args) {
            Figure kolo = new Kolo(10);
            Figure prostokat = new Prostokąt(1,5);
            Figure kwadrat = new Kwadrat(5);

           List figury = new ArrayList();
            figury.add(kolo);
            figury.add(prostokat);
            figury.add(kwadrat);


            for (int i = 0; i < figury.size(); i++) {
                if(figury.get(i) instanceof Figure){
                    System.out.println(
                            ((Figure) figury.get(i))
                                    .obliczObwod());
                    System.out.println(
                            ((Figure) figury.get(i))
                                    .obliczPole());


                }
            }

    }

}

