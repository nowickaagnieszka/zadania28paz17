package Zad5;

public class Kwadrat implements Figure{
    private double a;

    public Kwadrat(double a) {
        this.a = a;
    }

    @Override
    public double obliczPole() {
        return a*a;
    }

    @Override
    public double obliczObwod() {
        return 4 * a;
    }
}
