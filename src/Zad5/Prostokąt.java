package Zad5;

public class Prostokąt implements Figure {
    private int a;
    private int b;

    public Prostokąt(int a, int b) {
        this.a = a;
        this.b = b;
    }


    @Override
    public double obliczPole() {
        return a*b;
    }

    @Override
    public double obliczObwod() {
        return 2* a+2*b;
    }

}
