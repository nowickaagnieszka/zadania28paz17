package Zad5;

public class Kolo implements Figure {
    private static final double LICZBA_PI = 3.14d; //piszemy z duzych jak enumy
    private final double promien;

    public Kolo(double promien) {
        this.promien = promien;
    }

    @Override
    public double obliczPole() {
        return LICZBA_PI * promien * promien;
    }

    @Override
    public double obliczObwod() {
        return LICZBA_PI * promien * 2;
    }

}
