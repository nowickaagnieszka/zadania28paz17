package Zad17;

import java.util.Scanner;

/*
Napisz generator (klasa z metodą “generate...()”): (gdzie trzy kropki to następne słowo,
np. w przykładzie a, będzie to generateNumber())
        a) Kolejnych liczb.
        b) Kolejnych liczb w podanym przedziale. (przedział podawany w konstruktorze)
        c) Kolejnych liczb parzystych.
        d) Kolejnych wielokrotności liczby podanej w parametrze.
        e) Występujących na przemian wartości true, false.
        h) Kolejnych liter alfabetu (znaków unicode od a do z).
 */
public class Main17 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Generator generator = new Generator(2, 8);

        String line;
        do {
            System.out.println("Give command:");
            line = scanner.nextLine();

            if (line.equals("next")) {
                System.out.println(generator.generateNumber());
            } else if (line.equals("nextinrange")) {
                System.out.println(generator.generateNumberInRange());
            } else if (line.equals("nextinrange2")) {
                System.out.println(generator.generateNumberInRange2());
            } else if (line.equals("nexteven")) {
                System.out.println(generator.generateNextEven());
            } else if (line.equals("nexttruefalse")) {
                System.out.println(generator.generateTrueFalse());
            } else if (line.equals("nextaz")) {
                System.out.println(generator.generateChar());
            } else if (line.equals("reset")) {
                generator = new Generator(20, 80);
            } else if (line.equals("nextmul")) {
                System.out.println("Give me multiplication number:");
                String line2 = scanner.nextLine();
                System.out.println("You've given: " + line2);
                try {
                    int number = Integer.parseInt(line2);
                    System.out.println(generator.generateNextMult(number));
                } catch (NumberFormatException nfe) {
                    System.out.println("Wrong number format");
                }
            }else{
                System.out.println("Wrong command");
            }


        } while (!line.equals("quit"));

    }

}

