package Zad1;
/*
    Dodaj klasę Osoba o polach imie i wiek.
    Utwórz 3 osoby o różnych imionach i wieku.
    Wypisz ich dane w postaci:
    “Jestem <imie>, mam <wiek> lat.”
        a) stwórz do tego metodę printYourNameAndAge
        b) wypisz wartości używając metody toString
        c) wypisz dane używając metod getterów i setterów
*/

public class Main1 {
    public static void main(String[] args) {
        Osoba osoba1 = new Osoba("Ania",23 );
        Osoba osoba2 = new Osoba("Franek", 22);
        Osoba osoba3 = new Osoba("Kasia", 31);

        osoba1.printYourNameAndAge();

        System.out.println(osoba2.toString());

        System.out.println("Jestem " + osoba3.getImie() + ", mam " + osoba3.getWiek() + " lat.");
    }

}
