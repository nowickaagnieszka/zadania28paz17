package Zad1;

import java.io.PrintStream;

public class Osoba {
    private String imie;
    private int wiek;

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public Osoba(String imie, int wiek) {
        this.imie = imie;
        this.wiek = wiek;
    }
    //a
    public void printYourNameAndAge(){
        System.out.println("Jestem " + imie + ", mam " + wiek + " lat.");
        System.out.println("Imie"+ getImie()+ " wiek: "+ getWiek());
    }
    //b

    @Override
    public String toString() {
        return "Jestem " + imie + ", mam " + wiek + " lat.";

    }
}
