package DijkstraExample;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class Graph {
    private Map<Integer, Node> nodesMap = new HashMap<>();

    public void addNode(Node... nodes) {
        for (Node node : nodes) {
            nodesMap.put(node.getId(), node);
        }
    }

    public void dijkstraFrom(Node start, Node finish) {
        /*
            dla każdego wierzchołka v w V[G] wykonaj
            d[v] := nieskończoność
            poprzednik[v] := niezdefiniowane
         */
        for (Node n : nodesMap.values()) {
            n.setDistanceFromStart(Double.POSITIVE_INFINITY);
        }
        // d[s] := 0
        start.setDistanceFromStart(0);

        PriorityQueue<QueueElement> queue = new PriorityQueue<>(new QueueElementComparator());
        queue.add(new QueueElement(0, start.getId(), String.valueOf(start.getId())));

        QueueElement top = null;
        // dopóki Q niepuste wykonaj
        while (!queue.isEmpty()) {
            // u := Zdejmij_Min(Q)
            top = queue.poll();
            double obecnyDystans = top.getCost();

            Node n = nodesMap.get(top.getNodeId()); // wierzcholek z kolejki
            if (n.getDistanceFromStart() < obecnyDystans) {
                continue;
            }
            n.setDistanceFromStart(obecnyDystans);

            if(n == finish){
                break;
            }

            // dla każdego wierzchołka v – sąsiada u wykonaj
            for (Edge edge : n.getNeighbours()) {
                int idSasiada = edge.getId_to();
                Node neighbour = nodesMap.get(idSasiada);

                // jeżeli d[v] > d[u] + w(u, v) to
                double kosztDrogiDoSasiada = (obecnyDystans + edge.getCost());
                if (neighbour.getDistanceFromStart() > kosztDrogiDoSasiada) {
                    QueueElement newNodeToVisit = new QueueElement(
                            kosztDrogiDoSasiada,
                            neighbour.getId(),
                            top.getPath() + "->" + neighbour.getId()
                    );
                    queue.add(newNodeToVisit);
                }
            }
        }

        System.out.println("Dystans do wierzcholka: " + finish.getDistanceFromStart());
        System.out.println("Trasa: " +top.getPath());
    }

    class QueueElement {
        private double cost;
        private int nodeId;
        private String path;

        public QueueElement(double cost, int nodeId, String path) {
            this.cost = cost;
            this.nodeId = nodeId;
            this.path = path;
        }

        public double getCost() {
            return cost;
        }

        public void setCost(double cost) {
            this.cost = cost;
        }

        public int getNodeId() {
            return nodeId;
        }

        public void setNodeId(int nodeId) {
            this.nodeId = nodeId;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }
    }

    class QueueElementComparator implements Comparator<QueueElement> {
        @Override
        public int compare(QueueElement e1, QueueElement e2) {
            if (e1.getCost() == e2.getCost()) {
                return 0;
            }
            if (e1.getCost() > e2.getCost()) {
                return 1;
            } else {
                return -1;
            }
        }
    }
}