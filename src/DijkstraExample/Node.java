package DijkstraExample;
import java.util.HashSet;
import java.util.Set;

public class Node {
    private int id;
    private boolean visited;
    private Set<Edge> neighbours = new HashSet<>();

    //
    private double distanceFromStart;

    public Node(int id) {
        this.id = id;
    }

    public void addEdge(double cost, int to){
        neighbours.add(new Edge(cost,id, to));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public Set<Edge> getNeighbours() {
        return neighbours;
    }

    public void setNeighbours(Set<Edge> neighbours) {
        this.neighbours = neighbours;
    }

    public double getDistanceFromStart() {
        return distanceFromStart;
    }

    public void setDistanceFromStart(double distanceFromStart) {
        this.distanceFromStart = distanceFromStart;
    }
}