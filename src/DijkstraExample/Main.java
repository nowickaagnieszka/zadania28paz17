package DijkstraExample;
public class Main {
    public static void main(String[] args) {
        Node n1 = new Node(1);
        n1.addEdge(2, 8);
        n1.addEdge(4, 2);
        n1.addEdge(3, 6);
        n1.addEdge(1, 5);

        Node n2 = new Node(2);
        n2.addEdge(4, 1);
        n2.addEdge(1, 8);
        n2.addEdge(1, 3);

        Node n3 = new Node(3);
        n3.addEdge(1, 2);
        n3.addEdge(2, 8);
        n3.addEdge(2, 4);

        Node n4 = new Node(4);
        n4.addEdge(2, 3);
        n4.addEdge(2, 6);
        n4.addEdge(5, 7);

        Node n5 = new Node(5);
        n5.addEdge(1, 1);
        n5.addEdge(10, 7);

        Node n6 = new Node(6);
        n6.addEdge(3, 1);
        n6.addEdge(2, 4);

        Node n7 = new Node(7);
        n7.addEdge(10, 5);
        n7.addEdge(5, 4);

        Node n8 = new Node(8);
        n8.addEdge(2, 1);
        n8.addEdge(1, 2);
        n8.addEdge(2, 3);

        Graph g = new Graph();
        g.addNode(n1, n2, n3, n4, n5, n6, n7, n8);

        g.dijkstraFrom(n1, n7);
    }
}