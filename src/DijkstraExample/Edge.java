package DijkstraExample;

public class Edge {
    private double cost;
    private int id_from;
    private int id_to;

    public Edge(double cost, int id_from, int id_to) {
        this.cost = cost;
        this.id_from = id_from;
        this.id_to = id_to;
    }

    public double getCost() {
        return cost;
    }

    public int getId_from() {
        return id_from;
    }

    public int getId_to() {
        return id_to;
    }
}