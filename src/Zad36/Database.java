package Zad36;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Database {
    Map<Integer, Record> dane = new HashMap <>();

    public Database() {
    }

    public void addRecord(Record r) {
        dane.put(r.getIdentyfikator(), r);
    }

    public void refresh() {
        Iterator<Record> it = dane.values().iterator();
        while(it.hasNext()) {
            Record r = it.next();
            LocalDateTime kopiaPrzesunieta = r.getCzas_dodania().plusMinutes(r.getCzas_waznosci());
            if (LocalDateTime.now().isAfter(kopiaPrzesunieta)) {
                it.remove();
            }
        }
    }

    public void print(){
        for(Record r : dane.values()){
            System.out.println(r);
        }
    }
}
