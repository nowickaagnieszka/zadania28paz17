package Zad36;

import java.time.LocalDateTime;

public class Record {
    private LocalDateTime czas_dodania;
    private long czas_waznosci;
    private int identyfikator;
    private String tekst;

    public Record(LocalDateTime czas_dodania, long czas_waznosci, int identyfikator, String tekst) {
        super();
        this.czas_dodania = czas_dodania;
        this.czas_waznosci = czas_waznosci;
        this.identyfikator = identyfikator;
        this.tekst = tekst;
    }

    public LocalDateTime getCzas_dodania() {
        return czas_dodania;
    }

    public void setCzas_dodania(LocalDateTime czas_dodania) {
        this.czas_dodania = czas_dodania;
    }

    public long getCzas_waznosci() {
        return czas_waznosci;
    }

    public void setCzas_waznosci(long czas_waznosci) {
        this.czas_waznosci = czas_waznosci;
    }

    public int getIdentyfikator() {
        return identyfikator;
    }

    public void setIdentyfikator(int identyfikator) {
        this.identyfikator = identyfikator;
    }

    public String getTekst() {
        return tekst;
    }

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }

    @Override
    public String toString() {
        return "Record [czas_dodania=" + czas_dodania + ", czas_waznosci=" + czas_waznosci + ", identyfikator="
                + identyfikator + ", tekst=" + tekst + "]";
    }
}
