package Zad3;

public class DziennikSzkolny {
    private String[] dziennik;
    // private String[] studentList = new String[30]; --- pozwoli nam nie powtarzac w kazdym konstruktorze get/set
    private int iloscUczniow;

    public DziennikSzkolny() {
        this.dziennik = new String[30];
        this.iloscUczniow = 0;
    }

    public boolean dodajUcznia(String uczen){
        if(iloscUczniow<30){
            this.dziennik[iloscUczniow] = uczen;
            iloscUczniow = iloscUczniow +1;
        } else {
            return false;
        }

        return true;

    }
    public boolean usunUcznia(int nrUcznia){
        if(iloscUczniow<nrUcznia){
            return false;
        }else {
            dziennik[nrUcznia-1]= null;
            for (int i = nrUcznia-1; i <iloscUczniow ; i++) {
                this.dziennik[i] = this.dziennik[i+1];
                iloscUczniow = iloscUczniow -1;
            }
        }
        return true;
    }
    public void printAll(){
        for (int i = 0; i < iloscUczniow; i++) {
            System.out.println(dziennik[i]);
        }
    }
}
