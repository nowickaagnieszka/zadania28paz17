package Zad3;
/*
    Stwórz klasę która reprezentuje dziennik w szkole.
    Zakładamy że dziennik ma zawsze 30 miejsc uczniów.
    Każdy dziennik posiada pole - ilość wpisanych uczniów.
    Dodaj metodę wypisania wszystkich uczniów.

    (Zakładamy że uczniowie to po prostu ich imiona i nazwiska - tablica string'ów)
    (Stwórz metodę dodania ucznia i usunięcia ucznia)
 */
public class Main3 {
    public static void main(String[] args) {
        DziennikSzkolny klasa = new DziennikSzkolny();
        klasa.dodajUcznia("Marek Kowalski");
        klasa.dodajUcznia("Anna Zielona");
        klasa.dodajUcznia("Dorota Nowak");
        klasa.printAll();
        System.out.println();
        klasa.usunUcznia(2);
        klasa.printAll();
    }
}
