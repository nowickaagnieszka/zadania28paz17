package Zad31;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

/*
Stwórz aplikację która na:
        - wpisanie date wypisuje obecny LocalDate
        - wpisanie time wypisuje obecny LocalTime
        - wpisanie datetime wypisuje obecny LocalDateTime
        (W wybranym przez Ciebie formacie)
    Jeśli użytkownik wpisze 'quit' to zakoncz program.

Daty, wskazówki:
Sposób na czas (Java8):
        LocalDateTime czasWjazd = LocalDateTime.now();
        Timestamp tWjazd = Timestamp.valueOf(czasWjazd);
        LocalDateTime czasWyjazd = LocalDateTime.now();
        Timestamp tWyjazd = Timestamp.valueOf(czasWyjazd);
        Long roznicaT = tWyjazd.getTime() - tWjazd.getTime();

Wypisywanie daty w odpowiednim formacie:
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        System.out.println(czasWjazd.format(formatter));

Dostępne znaki dla DateTimeFormatter'a:
https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html
 */
public class Main31 {
    public static void main(String[] args) {
        Scanner scr = new Scanner(System.in);

        LocalDate data;
        LocalTime czas;
        LocalDateTime dataICzas;
        DateTimeFormatter formatter;
        String cmd;

        while(true) {
            System.out.println("Co chcesz zrobić? Dostępne polecenia:\n date - wypisanie obecnej daty\n" +
                    " time - wypisanie obecnej godziny\n datetime - wypisanie daty i godziny\n quit - wyjście z programu");
            cmd = scr.nextLine();
            if(cmd.equalsIgnoreCase("date")){
                formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                data = LocalDate.now();
                System.out.println(data.format(formatter));

            } else if(cmd.equalsIgnoreCase("time")){
                formatter = DateTimeFormatter.ofPattern("hh:mm");
                czas = LocalTime.now();
                System.out.println(czas.format(formatter));
            } else if(cmd.equalsIgnoreCase("datetime")){
                formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd hh:mm:ss");
                dataICzas = LocalDateTime.now();
                System.out.println(dataICzas.format(formatter));
            } else if(cmd.equalsIgnoreCase("quit")){

                System.out.println("Bye bye!");
                break;
            }

        }

    }
}
