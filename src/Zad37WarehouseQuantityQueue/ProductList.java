package Zad37WarehouseQuantityQueue;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

public class ProductList {

    private PriorityQueue<ProductQuantity> queue;

    public ProductList() {
        this.queue = new PriorityQueue<>(new ProductComparator());
    }

    public void addProduct(ProductQuantity productQuantity) {
        queue.add(productQuantity);
    }

    public void removeProduct(int quantityToRemove) {

        while (quantityToRemove > 0) {
            ProductQuantity product = queue.poll();
            if (product == null) {
                System.out.println("Nie mam produktu");
                break;
            }
            int productQuantity = product.getQuantity();
            if (productQuantity < quantityToRemove) {
                // niewystarczająca ilość
                int difference = quantityToRemove - productQuantity;
                System.out.println("Brakuje " + difference + " produktów tego typu.");
                quantityToRemove -= productQuantity;
                // produkt nie jest umieszczany z powrotem w kolejce
            } else {
                productQuantity -= quantityToRemove;
                quantityToRemove = 0;
                product.setQuantity(productQuantity);
                queue.add(product);
                // umieszczam go jeszcze raz w kolejce
            }
            System.out.println("Zmniejszyłem ilość produktu: " + this);
        }
    }

    public void listProductsFromThisList() {
//        for (ProductQuantity quantity : queue) {
//            System.out.print(quantity.getProduct());
//            if (quantity.getProduct().getExpirationDate().isBefore(LocalDateTime.now())) {
//                System.out.print(" - przeterminowany");
//            }
//            System.out.println();
//        }
//        while (!queue.isEmpty()) {
//            ProductQuantity quantity = queue.poll();
//            System.out.print(quantity.getProduct());
//            if (quantity.getProduct().getExpirationDate().isBefore(LocalDateTime.now())) {
//                System.out.print(" - przeterminowany");
//            }
//            System.out.println();
//        }

        ProductQuantity[] products = new ProductQuantity[queue.size()];
        products = queue.toArray(products);
        Arrays.sort(products);
        for (ProductQuantity quantity : products) {
            System.out.print(quantity.getQuantity() + "x " + quantity.getProduct());
            if (quantity.getProduct().getExpirationDate().isBefore(LocalDateTime.now())) {
                System.out.print(" - przeterminowany");
            }
            System.out.println();
        }
    }

    class ProductComparator implements Comparator<ProductQuantity> {
        @Override
        public int compare(ProductQuantity p1, ProductQuantity p2) {
            // 1 - p1 wieksze od p2
            // 0 - rowne
            // -1 - p2 wieksze od p1
            return p1.compareTo(p2);
        }
    }

    @Override
    public String toString() {
        return "ProductList{" +
                "queue=" + queue +
                '}';
    }
}