package Zad37WarehouseQuantityQueue;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        boolean isWorking = true;

        //23.10.2017-18:00
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy-HH:mm");

        // warehouse
        Warehouse warehouse = new Warehouse();
        while (isWorking) {
            String line = sc.nextLine().trim().toLowerCase();

            if (line.equals("quit")) {
                break;
            }
            if (line.startsWith("dodaj ")) {
                line = line.replaceFirst("dodaj ", "");
                // nazwa cena ilosc data
                String[] words = line.split(" ");

                // dane produktu
                String name = words[0];
                Double price = Double.parseDouble(words[1]);
                Integer quantity = Integer.parseInt(words[2]);
                LocalDateTime expirationDate = LocalDateTime.parse(words[3], formatter);

                // dodanie do warehouse
                warehouse.addProduct(quantity, name, price, expirationDate);
            } else if (line.startsWith("listuj")) {
                warehouse.listProducts();
            } else if (line.startsWith("sprawdz")) {
                warehouse.check();
            } else if (line.startsWith("usun ")) {
                line = line.replaceFirst("usun ", "");

                // nazwa ilosc
                String[] words = line.split(" ");

                // dane produktu
                String name = words[0];
                Integer quantity = Integer.parseInt(words[1]);

                warehouse.removeProduct(quantity, name);
            } else {
                System.out.println("Nada");
            }
        }

        System.out.println("Aplikacja zakończona.");
    }
}