package Zad37WarehouseQuantityQueue;
import java.util.Stack;

public class StackBinaryWriter {
    public String getBinaryOf(int number) {
        Stack<Character> stack = new Stack();

        while (number >= 1) {
            if (number == 1) {
                stack.push('1');
                break;
            } else {
                if (number % 2 == 0) {
                    stack.push('0');
                } else {
                    stack.push('1');
                }
                number /= 2;
            }
        }

        StringBuilder sb = new StringBuilder();
        while (!stack.isEmpty()) {
            sb.append(stack.pop());
        }

        return sb.toString();
    }
}

