package Zad37WarehouseQuantityQueue;
import java.util.Scanner;

public class MainBinary {
    public static void main(String[] args) {
        StackBinaryWriter writer = new StackBinaryWriter();
        Scanner sc = new Scanner(System.in);
        boolean work = true;
        while (work) {
            int number = sc.nextInt();

            System.out.println(writer.getBinaryOf(number));
        }
    }
}