package Zad22;

import java.util.ArrayDeque;
import java.util.Queue;

public class Gracz {
    Queue<Karta> lista = new ArrayDeque<>();

    private Karta aktualna;

    public Karta getAktualna() {
        return aktualna;
    }

    public void addCard(Karta card){
        lista.add(card);
    }

    public void dobierz(){
        System.out.println(lista.peek());
        aktualna=lista.poll();

    }

    public int size(){
        return lista.size();
    }
}
