package Zad22;

import java.util.ArrayList;
import java.util.List;

public class Talia {
    // Queue<Karta> lista = new ArrayDeque<>();
    List<Karta> lista = new ArrayList<>();

    public Talia() {
        for(Kolor color : Kolor.values()){
            for(Figura shape : Figura.values()){
                lista.add(new Karta(color,shape));
            }
        }
    }

}
