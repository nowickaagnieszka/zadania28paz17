package Zad22;

import java.util.Random;

public class PoleGry {
    Talia talia = new Talia();
    Gracz gracz = new Gracz();
    Gracz gracz2 = new Gracz();

    public void generujTalie(){
        Random random = new Random();
        for(int i = 0; i < 20; i++){
            int x = random.nextInt(20);
            gracz.lista.add(talia.lista.get(x));
        }
        for(int i = 0; i < 20; i++){
            int x = random.nextInt(20);
            gracz2.lista.add(talia.lista.get(x));
        }
    }

    public void walka(){
        if(gracz.getAktualna().figura.getValue() > gracz2.getAktualna().figura.getValue()){
            gracz.addCard(gracz.getAktualna());
            gracz.addCard(gracz2.getAktualna());
        }
        if(gracz.getAktualna().figura.getValue() < gracz2.getAktualna().figura.getValue()){
            gracz2.addCard(gracz2.getAktualna());
            gracz2.addCard(gracz.getAktualna());
        }
    }
}
