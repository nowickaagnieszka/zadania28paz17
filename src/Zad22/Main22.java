package Zad22;
/*
Symulacja gry w wojnę. (2 graczy, 52 karty)
 */
import java.util.Scanner;

public class Main22 {
    public static void main(String[] args) {
        PoleGry poleGry = new PoleGry();
        poleGry.generujTalie();
        Scanner scanner = new Scanner(System.in);
        String czyKontynuowac = "t";

        do{
            System.out.println("Gracz 1 wylosowal: ");
            poleGry.gracz.dobierz();
            System.out.println("Gracz 2 wylosowal: ");
            poleGry.gracz2.dobierz();
            poleGry.walka();
            System.out.println("Talia gracza 1 zawiera " + poleGry.gracz.size()+" kart");
            System.out.println("Talia gracza 2 zawiera " + poleGry.gracz2.size()+" kart");
            System.out.println("Kontynuowac? t/n");
            czyKontynuowac = scanner.next();
        }while(poleGry.gracz.size() > 0 && poleGry.gracz2.size() > 0 && czyKontynuowac.equals("t"));
    }
}
