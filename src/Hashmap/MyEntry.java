package Hashmap;

public class MyEntry {
    private String key;
    private Integer data;

    public MyEntry(String key, Integer data) {
        this.key = key;
        this.data = data;
    }

    public String getKey() {
        return key;
    }

    public Integer getData() {
        return data;
    }

    public void setData(Integer data) {
        this.data = data;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
