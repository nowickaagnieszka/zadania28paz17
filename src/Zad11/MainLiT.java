package Zad11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainLiT {
    public static void main(String[] args) {
        List<Integer> lista2 = new ArrayList<>(Arrays.asList(1, 2, 4, 2, 5, 12, 3, 2));
        int[] tablica = new int[]{4, 2, 2, 1, 5, 29, 3, 8};

        similarNo(lista2, tablica);
        List<Integer> lista3 = copyList(lista2);
        copyList(lista2);
        System.out.println("Kopiuje liste:" + lista3);

        int[] nowaTablica = copyToArray(lista2);
        System.out.println("Przypisuje liste do tablicy:");
        for (int i = 0; i < nowaTablica.length; i++) {

            System.out.print(nowaTablica[i] + ", ");
        }
        System.out.println();
        System.out.println("Przypisuje tablice do listy: ");
        List<Integer> lista4 = copyArrayToList(tablica);
        System.out.println(lista4);


        List<Integer> list = merge(lista4, lista2, lista2);
        System.out.println("Przepisuje dowolna ilosc list do jednej:"); //varargs (generycznie)
        System.out.println(list);
        boolean wynik = checkLists(lista2, lista4);
        System.out.print("Czy dwie listy sa takie same? " );
        if(wynik){
            System.out.print("tak");
        }else{
            System.out.print("nie");
        }
    }

    public static void similarNo(List<Integer> lista2, int[] tablica) {
        int licznik = 0;
        int powtorzenia = 0;
//        for (Integer spr : lista2 ) {
//            if(spr==tablica[licznik++] ){
//                powtorzenia++;
//                }
//        }
        for (int i = 0; i < lista2.size(); i++) {
            if (lista2.get(i) == tablica[i]) {
                powtorzenia++;
            }
        }
        System.out.println("Ilosc pozycji, na ktorych wartosci sa identyczne to " + powtorzenia);
        // TODO: metoda powinna zwracac ilosc powtorzen
        // PO WYKONANIU FOR! PO ZLICZENIU
    }

    public static List<Integer> copyList(List<Integer> staraLista) { //kopiuje liste
        List<Integer> nowaLista = new ArrayList<Integer>(staraLista.size());
        for (Integer item : staraLista) {
            nowaLista.add(item);
        }

        return nowaLista;
    }

    public static int[] copyToArray(List<Integer> staraLista) {        //przepisuje liste do tablicy
        int[] nowaTablica = new int[staraLista.size()];
        int i = 0;
        for (Integer item : staraLista) {
            nowaTablica[i] = item;
            i++;
        }
        return nowaTablica;
    }

    public static List<Integer> copyArrayToList(int[] staraTablica) {
        List<Integer> nowaLista = new ArrayList<Integer>(staraTablica.length);
        for (int i = 0; i < staraTablica.length; i++) {
            nowaLista.add(staraTablica[i]);
        }
        return nowaLista;
    }

    public static List<Integer> copy2Lists(List<Integer> staraLista1, List<Integer> staraLista2) {
        List<Integer> nowaLista = new ArrayList<Integer>(staraLista1.size() + staraLista2.size());
        for (Integer item : staraLista1) {
            nowaLista.add(item);
        }
        for (Integer item : staraLista2) {
            nowaLista.add(item);
        }
        return nowaLista;
    }

    //List<Integer> list = merge(lista4, lista2, lista2);
    public static <T> List<T> merge(List<T>... lists) { // lists to tablica list
        // trzeba stworzyc liste i przepisac do niej wszystkie elementy list z parametru
        List<T> allLists = new ArrayList<T>();
        // lists[0]  = lista4
        // lists[1] = lista2
        // lists[2] = lista2
        for (int i = 0; i < lists.length; i++) {
            List<T> pojedynczaLista = lists[i];
//            for (int j = 0; j < pojedynczaLista.size(); j++) {
//                allLists.add(pojedynczaLista.get(i));
//            }
            allLists.addAll(pojedynczaLista);
        }
        return allLists;
    }

    public static boolean checkLists(List<Integer> staraLista1, List<Integer> staraLista2) {
        List<Boolean> sprLista = new ArrayList<Boolean>(staraLista1.size() + staraLista2.size());
        for (int i = 0; i < staraLista1.size(); i++) {
            if (staraLista1.get(i) != staraLista2.get(i)) return false;
        }
        return true;
        // TODO: metoda powinna zwracac boolean - true - jesli sa takie same, false jesli nie
    }
}




