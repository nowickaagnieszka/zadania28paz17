package Zad11;
/* (LISTY - (na przykładzie ArrayList):  jak zad18.
        Napisz metody (użyj pętli for i forEach):
            - Liczącą sumę liczb na liście
            - Liczącą iloczyn liczb na liście
            - Liczącą średnią liczb na liście

        * Napisz metodę liczącą: //MainBoolean
            - Negujący wszystkie wartości logiczne na liście //a
            - Koniunkcję wartości logicznych na liście (&&)  //b
            - Alternatywę wartości logicznych na liście (||) //c

        ---------------------------------------------------------
        Napisz program tworzący dziesięcioelementową listę i tablice liczb,
        a następnie sprawdzający ilu pozycjach wartości są identyczne.
            Przykład:
            Lista   {1,2,4,2,5,12,3,2}
            Tablica {4,2,2,1,5,29,3,8}
                Wynik: 3         //MainLiT

        *Napisz metodę która:
            - kopiuje listę ( metoda która jako parametr przyjmuje liste i zwraca inną liste z tymi samymi elementami)
            - przepisuje listę do tablicy (podanej jako parametr) ( parametr = lista , zwraca = tablice)
            - przepisuje tablicę do listy (nowo utworzonej) ( parametr = tablica, zwraca liste )
            - przepisuje dwie listy do jednej ( parametrem są dwie listy, a zwraca jedna liste)
            - przepisuje dowolną ilość list do jednej (varargs) (parametr varargs list, zwraca liste)
            -* przepisuje dowolną ilość list do jednej (generycznie) (varargs) (parametr varargs list, zwraca liste)
            - sprawdza, czy dwie listy są takie same - (zwraca = true/false, przyjmuje dwie listy
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main11 {
    public static void main(String[] args) {
        int[] tab = new int[]{1, 5, 6, 7, 8, 9, 0};
//a
        int sum = 0;
        for (int i = 0; i < tab.length; i++) {
            sum += tab[i];
        }
        System.out.println(sum);
// z lista
        int sumListy = 0;
        List<Integer> list = new ArrayList<Integer>(Arrays.asList(1, 5, 6, 7, 8, 9, 0));
        for (int i = 0; i < list.size(); i++) {
            sumListy += list.get(i);

        }
        System.out.println(sumListy);

        //b

        int iloczyn = 1;
        List<Integer> list2 = new ArrayList<Integer>(Arrays.asList(1, 5, 6, 7, 8, 9, 7, 3, 4, 2, 8));
        for (int i = 0; i < list.size(); i++) {
            iloczyn *= list2.get(i);
        }
        System.out.println(iloczyn);

        //b2

        int iloczyn2 = 1;
        List<Integer> list3 = new ArrayList<Integer>(Arrays.asList(1, 5, 6, 7, 8, 9, 7));
        for (Integer x : list3) {
            iloczyn2 *= x;
        }
        System.out.println(iloczyn2);


        //c
        System.out.println(sumListy / list.size());
    }
}



