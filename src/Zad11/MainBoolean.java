package Zad11;

import java.util.ArrayList;
public class MainBoolean {

    public static void main(String[] args) {
        ArrayList<Boolean> booleans = new ArrayList<>();
        booleans.add(false);
        booleans.add(true);
        booleans.add(true);
        booleans.add(true);
        booleans.add(false);
        booleans.add(false);
        booleans.add(false);
        booleans.add(false);
        booleans.add(false);
        booleans.add(false);
        System.out.println(booleans);

        // Negacja
        for (int i = 0; i < booleans.size(); i++) {
            boolean wartosc = booleans.get(i);
            booleans.remove(i);
            booleans.add(i, !wartosc);
        }
        System.out.println(booleans);

//        ArrayList<Boolean> listaPrzepisana = new ArrayList<>();
//        for (Boolean wartosc : booleans) {
//            listaPrzepisana.add(!wartosc);
//        }
//        System.out.println(listaPrzepisana);


        // Koniunkcja, mnozenie, obydwa musza byc true, zeby bylo true, inne false
        boolean wynikKoniunkcji = true;
        for (Boolean wartosc : booleans) {
            wynikKoniunkcji = wynikKoniunkcji && wartosc;
        }
        System.out.println("Koniunkcja: " + wynikKoniunkcji);

        // Alternatywa, jezeli przynajmniej jedno jest true to jest true
        boolean wynikAlternatywy = false;
        for (Boolean wartosc : booleans) {
            wynikAlternatywy = wynikAlternatywy || wartosc;
        }
        System.out.println("Alternatywa: " + wynikAlternatywy);
    }
}
