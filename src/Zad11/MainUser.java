package Zad11;
/*
Napisz klasę User o polach String name, String password. Utwórz listę typu User,
dodaj do niej kilka obiektów, a następnie wypisz.
 */
import java.util.ArrayList;

public class MainUser {
    public static void main(String[] args) {
        User u1 = new User("Username1", "password1");
        User u2 = new User("Username2", "password2");
        User u3 = new User("Username3", "password3");
        User u4 = new User("Username4", "password4");

        ArrayList<User> listOfUsers = new ArrayList<>();

        listOfUsers.add(u1);
        listOfUsers.add(u2);
        listOfUsers.add(u3);
        listOfUsers.add(u4);

        System.out.println(listOfUsers);
    }
}
