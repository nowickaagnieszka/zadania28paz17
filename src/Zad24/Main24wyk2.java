package Zad24;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Main24wyk2 {
    public Set<Integer> tablica = new HashSet<>();
    public void init() {
        tablica.addAll(Arrays.asList(10, 12, 10, 3, 4, 12, 12, 300, 12, 40, 55));
    }
    public void print(){
        System.out.println("Liczba elementow: " +tablica.size());
        for (Integer e: tablica){
            System.out.printf("%d, ", e);
        }

        System.out.println();
    }
    public boolean containDuplicates(String word){
        Set<String> array = new HashSet<>();
        array.addAll(Arrays.asList(word.toLowerCase().split(" ")));

        return word.length() !=array.size();
    }
    public static void main(String[] args) {
        Main24wyk2 main = new Main24wyk2();
        main.init();
        main.print();

        main.tablica.remove(10);
        main.tablica.remove(12);

        main.print();
        System.out.println(main.containDuplicates("Kot ma"));

        Set<ParaLiczb> para = new HashSet<>();
        para.add(new ParaLiczb(1, 2));
        para.add(new ParaLiczb(5,2));
        para.add(new ParaLiczb(3, 3));

        for (ParaLiczb e: para){
            System.out.println(e);
        }


    }

}
