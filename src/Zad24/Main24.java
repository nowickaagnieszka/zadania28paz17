package Zad24;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/*
a. Umieść wszystkie elementy tablicy {10,12,10,3,4,12,12,300,12,40,55} w zbiorze (HashSet) oraz:
b. Wypisz liczbę elementów na ekran (metoda size())
c. Wypisz wszystkie zbioru elementy na ekran (forEach)
d. Usuń elementy 10 i 12, wykonaj ponownie podpunkty a) i b)
e. Napisz metodę sprawdzającą, czy w tekście nie powtarzają się litery z wykorzystaniem zbioru.
    (boolean containDuplicates(String text))
f. Utwórz klasę ParaLiczb (int a,int b) i dodaj kilka instancji do zbioru:
                {(1,2), (2,1), (1,1), (1,2)}.
g. Wyświetl wszystkie elementy zbioru na ekran. Czy program działa zgodnie z oczekiwaniem?

h*. Napisz metodę, która przyjmuje dowolną liczbę liczb typu double (varargs) i tworzy zbiór (HashSet),
    składający się z tych elementów, lub wyrzuca IllegalArgumentException, jeżeli elementy nie są unikalne
           public static Set<Double> newSet(double numbers…)
            - spróbuj przerobić metodę na wersję generyczną
                public static <T> Set<T> newSet(T... data)
 */
public class Main24 {
    public static void main(String[] args) {
        HashSet<Integer> hashSet = new HashSet<>();
        Integer[] tablica = new Integer[] {10,12,10,3,4,12,12,300,12,40,55};
        // można tak: hashSet.addAll(Arrays.asList(tablica));
        // lub for'em, jak poniżej:

        for (int i = 0; i <tablica.length ; i++) {
            hashSet.add(tablica[i]);
        }
        System.out.println(hashSet.size() + " size");
        for (Integer h:hashSet) {
            System.out.print(h + " ");
        }
    }
}
