package Zad6;

public class BankAccount {
    private double money;
    public BankAccount(){

    }

    public BankAccount(double money) {
        this.money = money;
    }
    public void addMoney(double howMuchToAdd){
        money = money - howMuchToAdd;
    }
    public void subMoney(double howMuchToSubstract){
        money = money - howMuchToSubstract;
    }
    public double getMoney(){
        return money;
    }
}
