package Zad6;
/*
Stwórz program a w nim klasę "BankAccount".
        konto bankowe powinno mieć metodę dodawania pieniędzy do konta, oraz odejmowania :
        (metoda addMoney która w parametrze przyjmuje ilosc pieniedzy do dodania oraz substractMoney,
         która jako parametrz przyjmuje ilosc pieniedzy do odjecia).
        Dodaj do klasy metodę "printBankAccountStatus" która powinna wypisywać stan konta.
 */
public class Main6 {
    public static void main(String[] args) {
    BankAccount account = new BankAccount();

    account.addMoney(20.0);
    account.subMoney(10.0);

        System.out.println("Na koncie jest: "+account.getMoney());
    }
}
