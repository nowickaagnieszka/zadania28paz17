package Zad14;

public class Paczka {
    private String odbiorca;
    private String nadawca;
    private boolean jestWyslana;
    private boolean jestZawartosc;

    public Paczka() {
    }

    public Paczka(String odbiorca, boolean jestZawartosc) {
        this.odbiorca = odbiorca;
        this.jestZawartosc = jestZawartosc;
    }

    public String getOdbiorca() {
        return odbiorca;
    }

    public void setOdbiorca(String odbiorca) {
        this.odbiorca = odbiorca;
    }

    public String getNadawca() {
        return nadawca;
    }

    public void setNadawca(String nadawca) {
        this.nadawca = nadawca;
    }

    public boolean isJestWyslana() {
        return jestWyslana;
    }

    public void setJestWyslana(boolean jestWyslana) {
        this.jestWyslana = jestWyslana;
    }

    public boolean isJestZawartosc() {
        return jestZawartosc;
    }

    public void setJestZawartosc(boolean jestZawartosc) {
        this.jestZawartosc = jestZawartosc;
    }

    public void wyslij() {
        if (!jestWyslana && odbiorca != null && jestZawartosc) {
            // moge wyslac
            System.out.println("Wysylam paczke");
            jestWyslana = true;
        } else if (jestWyslana) {
            System.out.println("Paczka była juz wyslana.");
        } else {
            System.out.println("Nie moge wyslac paczki brak odbiorcy/zawartosci.");
        }
    }

    public void wyslijPolecony() {
        if (!jestWyslana) {
            if (nadawca != null) {
                // mozemy wyslac polecony
                wyslij();
            } else {
                System.out.println("Nie mozesz wyslac paczki poleconej bez podania nadawcy");
                // nie mozemy
            }
        }
    }
}
