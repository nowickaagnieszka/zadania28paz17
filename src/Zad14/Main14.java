package Zad14;
/*
Stwórz klasę 'paczka' która:
        - ma pole odbiorca
        - ma pole nadawca
        - ma pole 'czy została wysłana'
        - ma flagę 'czy jest zawartość'
        - dodaj gettery i settery
    Przetestuj aplikację w Main22'ie.
        - dodaj domyślny (pusty) konstruktor, oraz konstruktor z odbiorcą i flagą zawartość,
        - ma metodę 'wyślij' która sprawdza czy jest zawartość, i czy jest odbiorca i wyświetla komunikat o nadaniu,
         jeśli jest zawartość i odbiorca oraz ustawia flagę 'czy została wysłana' na true.
        - ma metodę 'wyślij polecony' która sprawdza to samo co wyżej, ale sprawdza również czy
            nadawca jest ustawiony i pozwala wysłać tylko jeśli wszystkie pola są ustawione.
    Przetestuj aplikację w Main22'ie.
 */
public class Main14 {
    public static void main(String[] args) {
            Paczka paczka = new Paczka("Odbiorca", true);
            Paczka p = new Paczka();

//        paczka.wyslij();
            paczka.wyslijPolecony();

//        p.wyslij();
            p.wyslij();
            p.wyslij();
            p.wyslij();
        }

}

