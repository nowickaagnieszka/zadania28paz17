package Zad32;
/*
Utwórz interfejs Animal, który będzie zawierał metodę makeNoise();\
Utwórz klasę Dog i Cat, które będą implementowały ten interfejs za pomocą implements
 */
public class Main32 {
    public static void main(String[] args) {
        new Dog().makeNoise();

        new Cat().makeNoise();

        Cat c = new Cat();
        c.makeNoise();

        Animal animal = new Dog();
        animal.makeNoise();
    }

}
