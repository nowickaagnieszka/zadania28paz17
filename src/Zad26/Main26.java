package Zad26;
import java.util.*;

/*
a. Napisz program który prosi użytkownika o dodanie 5 liczb, a następnie umieszcza je na liście, którą wypisuje w pętli forEach
      b. Napisz program, który prosi użytkownika o napisanie zdania, dzieli to zdanie na słowa (metoda split),
        które umieszcza na liście, a następnie oblicza:
            - Ilość słów
            - Łączną ilość znaków w słowach
            - Srednią ilość znaków w słowach
        c. Napisz metodę, która tworzy nową listę podanych argumentów:
                public static List<Integer> newList(int… numbers)
        d*. Przekształć metodę na generyczną, tak aby mogła przyjmować argumenty dowolnego typu
 */
public class Main26 {
    public static void main(String[] args) {
        //a
        System.out.println("Podaj 5 liczb");
        ArrayList<Integer> integers = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 5; i++) {
            integers.add(scanner.nextInt());
        }
        integers.forEach(System.out::println);

        //b
        System.out.println("Podaj zdanie");
        Scanner scanner2 = new Scanner(System.in);
        String linia = scanner2.nextLine();
        String[] split = linia.split(" ");
        System.out.println("Ilosc slow: "+split.length);
        final int[] counter = {0};
        Arrays.stream(split).forEach(x-> counter[0] +=x.length());
            System.out.println("Ilosc znakow: "+ counter[0]);
            System.out.println("Srednia ilosc znakow: "+(counter[0] /split.length));


        }

    //c
    public static List<Integer> newList(Integer... numbers){
        List<Integer> intList = new ArrayList<>();
        for(Integer tmp:numbers){
            intList.add(tmp);
        }
        return intList;
    }

    //d w nowej klasie generycznej
    public static List<Object> newList2(Object... numbers){
        List<Object> objList = new ArrayList<>();
        for(Object tmp:numbers){
            objList.add(tmp);
        }
        return objList;
    }


}

