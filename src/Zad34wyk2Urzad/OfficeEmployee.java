package Zad34wyk2Urzad;

public class OfficeEmployee {
    private String name;

    public OfficeEmployee(String name) {
        super();
        this.name = name;
    }

    public void registerClient(Client klientDoZarejestrowania) {
        if (klientDoZarejestrowania == null) {
            System.out.println("Kolejka pusta ...");
            return;
        }
        if (!ZUSRegistry.checkIfRegistered(klientDoZarejestrowania.getPesel())) {
            ZUSRegistry.register(klientDoZarejestrowania);
            System.out.println("Zarejestrowałem klienta.");
        }else{
            System.out.println("Klient był już zarejestrowany.");
        }
    }
}
