package Zad34wyk2Urzad;

public class Client {
    private String name;
    private String pesel;

    public Client(String name, String pesel) {
        this.name = name;
        this.pesel = pesel;
    }

    public String getPesel() {
        return pesel;
    }
}
