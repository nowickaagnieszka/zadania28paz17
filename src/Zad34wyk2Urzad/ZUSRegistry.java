package Zad34wyk2Urzad;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ZUSRegistry {
    private static Map<String, Client> bazaDanych = new HashMap<>();

    public static void register(Client toRegister){
        bazaDanych.put(toRegister.getPesel(), toRegister);
    }

    public static boolean checkIfRegistered(String pesel){
        return bazaDanych.containsKey(pesel);
    }

    public static Collection<Client> getRegisteredClients(){
        return bazaDanych.values();
    }
}
