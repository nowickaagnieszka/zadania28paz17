package Zad25;

public class NoSuchStudentException extends RuntimeException {
    public NoSuchStudentException(String message) {
        super(message);
    }
}
