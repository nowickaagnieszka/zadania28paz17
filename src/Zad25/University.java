package Zad25;

import java.util.HashMap;
import java.util.Map;

public class University implements StudentFormat {
    private Map<Long, Student> mapa = new HashMap<>();

    public void addStudent(Student s) {
        mapa.put(s.getIndeks(), s);
    }

    public void addStudents(Student... students){
        for (Student st: students) {
            addStudent(st);
        }
    }

    public void addStudent(long indexNumber, String name, String surname) {
        mapa.put(indexNumber, new Student(indexNumber, name, surname));
    }

    public boolean containsStudent(long indeks) {
        return mapa.containsKey(indeks);
    }

    public Student getStudent(long indeks) {
        Student student = mapa.get(indeks);
        if (student == null) {
            throw new NoSuchStudentException("Nie znaleziono studenta z takim indeksem.");
        }
        return student;
    }

    public void printAllStudentVersion1() {
        for (Student st : mapa.values()) {
            System.out.println(st);
        }
    }

    public void printAllStudent() {
        for (Student st : mapa.values()) {
            System.out.println(format(st));
        }
    }

    @Override
    public String format(Student student) {
        return student.getIndeks() + " :  >> " + student.getNazwisko();
    }

}
