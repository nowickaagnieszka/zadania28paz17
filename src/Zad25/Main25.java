package Zad25;
/*  pobierz z zadania 'studenciaki'
a. Utwór klasę Student o polach: (long) numerIndeksu, imie, nazwisko oraz:
        b. Utwórz kilku studentów i dodaj do mapy (HashMap).
        c. sprawdź, czy mapa zawiera studenta o indeksie 100200,
        d. wypisz studenta o indeksie 100400
        e. wypisz liczbę studentów
        f. wypisz wszystkich studentów
        g**. zmień implementacje na TreeMap
        h. Do zadania pierwszego dodaj klasę University. Dodaj do niej metody:
                void addStudent(long indexNumber, String name, String surname)
                (możesz generować indexNumber)
                boolean containsStudent(long indexNumber)
                Student getStudent(long indexNumber)
                int studentsCount()
                void printAllStudent()
        i*. dodaj wyjątek NoSuchStudentException (RuntimeException), dodaj go do metody getStudent()
        j*. dodaj interfejs StudentFormat { String format(Student student) } i wykorzystaj go jako parametr w metodzie void printAllStudent()
        k. Wykonaj jeszcze raz zadanie pierwsze wykorzystując klasę University
 */
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main25{
    public static void main(String[] args) {
         Integer[] tablica = {1,2,3,4,55,6,77,8,999,};

            List<Integer> list = Arrays.asList(tablica);
            Set<Integer> set = new HashSet<>(list);
//        set.addAll(list);
//        for (Integer element : tablica) {
//            set.add(element);
//        }

            String a = "abc, def, gef";
            String wynik = a.replace(".", "").replace(",", "");

        }
}

