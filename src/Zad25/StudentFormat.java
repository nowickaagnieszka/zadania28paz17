package Zad25;

public interface StudentFormat {
    String format(Student student);
}
