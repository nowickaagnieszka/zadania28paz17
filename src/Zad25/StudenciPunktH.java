package Zad25;

public class StudenciPunktH {
    public static void main(String[] args) {
        University university = new University();

        Student sa = new Student(123123L, "a", "A");
        Student sb = new Student(100200L, "b", "B");
        Student sc = new Student(123125L, "c", "C");
        Student sd = new Student(123126L, "d", "D");
        Student se = new Student(123127L, "e", "E");

        university.getStudent(100201L);

        university.addStudents(sa, sb, sc, sd, se);

        university.printAllStudentVersion1();

        System.out.println(university.containsStudent(100200L));

        university.printAllStudent();

        printStudentUsing(new StudentFormat() {
            @Override
            public String format(Student student) {
                return student.getIndeks() + " <<<";
            }
        }, sa);

    }

    public static void printStudentUsing(StudentFormat format, Student st) {
        format.format(st);
    }
}
