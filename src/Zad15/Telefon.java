package Zad15;

public class Telefon {
    private boolean czyDzwoni = false;
    private String numerTelefonu;
    private String zawartoscWyswietlacza;
    private String wybranyNumer;


    public Telefon(String numerTelefonu) {
        this.numerTelefonu = numerTelefonu;
    }

    public boolean isCzyDzwoni() {
        return czyDzwoni;
    }

    public void setCzyDzwoni(boolean czyDzwoni) {
        this.czyDzwoni = czyDzwoni;
    }

    public String getNumerTelefonu() {
        return numerTelefonu;
    }

    public void setNumerTelefonu(String numerTelefonu) {
        this.numerTelefonu = numerTelefonu;
    }

    public void zadzwon() {
        if (!czyDzwoni) {
            czyDzwoni = true;
            System.out.println("Dzwonie gdzies");
        } else {
            System.out.println("Juz gdzies dzwonie");
        }
    }

    public void rozlacz() {
        if (czyDzwoni) {
            czyDzwoni = false;
            System.out.println("Rozlaczam polaczeznie");
        } else {
            System.out.println("Nie moge rozlaczyc, nie ma polaczenia");
        }
    }

    //***
    public void zadzwon(String numerWybierany) {
        if (!czyDzwoni) {
            czyDzwoni = true;
//            zawartoscWyswietlacza = ;
            setZawartoscWyswietlacza("Rozmowa z " + numerWybierany);
            System.out.println("Dzwonie gdzies");
        } else {
            System.out.println("Juz gdzies dzwonie");
        }
    }

    private void setZawartoscWyswietlacza(String zawartoscWyswietlacza) {
        this.zawartoscWyswietlacza = zawartoscWyswietlacza;
    }

    public void rozlaczZGwiazdka() {
        if (czyDzwoni) {
            czyDzwoni = false;
            zawartoscWyswietlacza = "";
            System.out.println("Rozlaczam polaczeznie");
        } else {
            System.out.println("Nie moge rozlaczyc, nie ma polaczenia");
        }
    }

}
