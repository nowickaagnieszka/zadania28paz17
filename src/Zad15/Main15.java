package Zad15;
/*
Stwórz klasę Telefon która posiada:
        a)
        - pole 'czy dzwoni'
        - pole 'numer telefonu'
        - metody getterów i setterów dla wszystkich pól
        - metodę 'zadzwon' ktora zmienia wartość pola 'czy dzwoni' z false na true,
                ale tylko wtedy kiedy jest false(nie mozna wykonac drugiego polaczenia jesli juz dzwoni)
        - metode 'rozlacz' ktora zmienia wartosc pola 'czy dzwoni' na false z true,
                ale tylko wtedy kiedy jest true (kiedy trwa jakas rozmowa)
    Przetestuj aplikację w Main22'ie
        b*)
        - pole 'zawartosc wyswietlacza' (String)
        - pole 'wybrany numer' (String)
        - zmodyfikuj metode zadzwon tak, zeby przyjmowala parametr (numer na ktory dzwonimy)
            po zmienieniu flagi 'czy dzwoni' na true ustaw 'wybrany numer' na numer z parametru.
            Ustaw pole 'zawartosc wyswietlacza' na tekst "Rozmowa z numer_telefonu".
    Przetestuj aplikację w Main22'ie
 */
public class Main15 {
    public static void main(String[] args) {
           Telefon telefon = new Telefon("123123");

            telefon.zadzwon("123123123");
            telefon.zadzwon("123123123");
            telefon.zadzwon();

        }

}
