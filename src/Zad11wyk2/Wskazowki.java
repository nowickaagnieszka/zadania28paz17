package Zad11wyk2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Wskazowki {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>(Arrays.asList(1,2,3,4,5));
        //na poczatku List dziala optymalniej, pozniej mozna LinkedList or ArrayList, dodawanie kolejnych zamiast
        // list.add(5);
        //list.add(15); itd. to (Arrays.asList( te numery po przecinku)
        list.add(0, 2);

        List<Integer> kopia = copyList(list);
        System.out.println(kopia.hashCode());

    }
    public static List<Integer> copyList(List<Integer>lista){
        List<Integer> kopia = new ArrayList<>(lista);
        return kopia;
    }
}
