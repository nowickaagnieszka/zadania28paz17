package Zad11wyk2;

import java.util.ArrayList;

public class ListMain11wyk2 {
    public static void main(String[] args) {
        ArrayList<Integer> lista = new ArrayList();
        lista.add(19);
        lista.add(18);
        lista.add(17);
        lista.add(16);
        lista.add(15);
        lista.add(14);
        lista.add(13);
        lista.add(12);
        lista.add(11);

        System.out.println(lista);
        for (int i = 0; i < lista.size(); i++) {
            System.out.println("Element o indeksie: " + i + " to: " + lista.get(i));
        }

        lista.remove(2);

        for (int i = 0; i < lista.size(); i++) {
            System.out.println("Element o indeksie: " + i + " to: " + lista.get(i));
        }

        Integer[] tablica = new Integer[]{1, 2, 3, 4, 5,6, 7, 8, 9, 10};

        for (int i = 0; i < tablica.length; i++) {
            System.out.println("Element tablicy o indeksie: " + i + " to: " + tablica[i]);
        }
        tablica[2] = null;

        for (int i = 0; i < tablica.length; i++) {
            System.out.println("Element tablicy o indeksie: " + i + " to: " + tablica[i]);
        }
    }

    public static <T> ArrayList<T> metoda(T... elementy) {
        ArrayList<T> list = new ArrayList<>();
        for (int i = 0; i < elementy.length; i++) {
            list.add(elementy[i]);
        }
        return list;
    }
}
