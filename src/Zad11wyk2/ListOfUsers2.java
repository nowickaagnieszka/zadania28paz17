package Zad11wyk2;


import java.util.ArrayList;

public class ListOfUsers2 {
    public static void main(String[] args) {
        ArrayList<User2> list = new ArrayList<>();

        list.add(new User2("a", 1));
        list.add(new User2("b", 2));
        list.add(new User2("c", 3));
        list.add(new User2("d", 4));
        list.add(new User2("e", 5));
        list.add(new User2("f", 6));

        System.out.println(list);

        User2 tenDrugi = list.get(1);
        tenDrugi.setName("abrakadabra");

        System.out.println(list);
    }
}