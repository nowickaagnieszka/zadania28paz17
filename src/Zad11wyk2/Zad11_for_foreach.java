package Zad11wyk2;

import java.util.List;

public class Zad11_for_foreach {
    public static void main(String[] args) {
        /**
         * Napisz metody (użyj pętli for i forEach):
         - Liczącą sumę liczb na liście
         - Liczącą iloczyn liczb na liście
         - Liczącą średnią liczb na liście
         */
    }

    // metoda liczaca sume w tablicy
    private static int sum(int[] tablica) {
        int sum = 0;
        for (int i = 0; i < tablica.length; i++) {
            // int wartosc = tablica[i];
            sum += tablica[i];
        }
        return sum;
    }

    // metoda liczaca sume elementow z tablicy // foreach
    private static int sumForeach(int[] tablica) {
        int sum = 0;
        for (int wartosc : tablica) {
            sum += wartosc;
        }
        return sum;
    }

    // metoda liczaca sume w listy
    private static int sumList(List<Integer> lista) {
        int sum = 0;
        for (int i = 0; i < lista.size(); i++) {
            // int wartosc = lista.get(i);
            sum += lista.get(i);
        }
        return sum;
    }

    // metoda liczaca sume elementow z listy // foreach
    private static int sumListForeach(List<Integer> lista) {
        int sum = 0;
        for (int wartosc : lista) {
            sum += wartosc;
        }
        return sum;
    }

    // metoda liczaca iloczyn elementów z listy
    private static int multiplyList(List<Integer> lista) {
        int result = 1;
        for (int i = 0; i < lista.size(); i++) {
            // int wartosc = lista.get(i);
            result *= lista.get(i);
        }
        return result;
    }

    // metoda liczaca iloczyn elementow z listy // foreach
    private static int multiplyListForeach(List<Integer> lista) {
        int result = 1;
        for (int wartosc : lista) {
            result *= wartosc;
        }
        return result;
    }

    // metoda liczaca średnia elementów listy (suma/ilość)
    private static double avgList(List<Integer> lista) {
        return (double) sumList(lista) / lista.size();
    }
}
