package Zad11wyk2;

import java.util.List;

public class Zad11_logic {
    public static void main(String[] args) {
        /**
 * Negacja elementów przez ustawianie konkretnego elementu na
 * jego przeciwną wartość, poprzez pobranie elementu,
 * następnie jego usunięcie i na końcu wstawienie w jego
 * miejsce jego znegowanej wartości.
 * @param list - lista elementów do znegowania.
 */
    }
        public static void negate(List<Boolean> list) {
            for (int i = 0; i < list.size(); i++) {
                // 1. pobranie
                boolean tmp = list.get(i);
                list.remove(i);
                list.add(i, !tmp);
            }
        }

        /**
         * Negacja elementów przez ustawianie konkretnego elementu na
         * jego przeciwną wartość, poprzez usunięcie elementu  (które
         * również zwraca jego wartość) a następnie wstawienie w jego
         * miejsce jego znegowanej wartości.
         * @param list - lista elementów do znegowania.
         */
        public static void negate2(List<Boolean> list) {
            for (int i = 0; i < list.size(); i++) {
                boolean tmp = list.remove(i);
                list.add(i, !tmp);
            }
        }

        /**
         * Negacja elementów przez ustawianie konkretnego elementu na
         * jego przeciwną wartość.
         * @param list - lista elementów do negowania.
         */
        public static void negate3(List<Boolean> list) {
            for (int i = 0; i < list.size(); i++) {
                list.set(i, !list.get(i));
            }
        }

        /**
         * Koniunkcja - weryfikuje czy wszystkie elementy listy są true.
         * Przykład:
         * Lista: true, true, true, true
         * Rozwiązanie: true && true && true && true => true
         * <p>
         * Lista: true, false, true, true
         * Rozwiązanie: true && false && true && true => false
         *
         * @param list - lista do sprawdzenia.
         * @return wynik koniunkcji wszystkich elementów.
         */
        public static boolean and(List<Boolean> list) {
            boolean result = true;
            for (Boolean value : list) {
                result &= value;
            }
            return result;
        }

        /**
         * Alternatywa - weryfikujemy czy którykolwiek z elementów jest true.
         * Przykład:
         * Lista: false, false, false, true
         * Rozwiązanie: false || false || false || true => true
         * <p>
         * Lista: false, false, false, false
         * Rozwiązanie: false || false || false || false => false
         *
         * @param list - lista do sprawdzenia
         * @return wynik alternatywy wszystkich elementów.
         */
        public static boolean alternative(List<Boolean> list) {
            boolean result = false;
            for (Boolean value : list) {
                result |= value;
            }
            return result;
        }
}
