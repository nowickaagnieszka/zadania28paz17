package Zad11wyk2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zad11_user {
    public static void main(String[] args) {
        // Tworzymy listę obiektów klasy User
        // więc w nawiasach <> - 'diamentowych' umieszczamy nazwę klasy
        // którą będzie przechowywać ta kolekcja.
       List<User> list = new ArrayList<>();
        // dodajemy obiekty do listy (cały zbiór)
        list.addAll(
                Arrays.asList(
                        new User("name1", "pass"),
                        new User("name2", "pass"),
                        new User("name3", "pass"),
                        new User("name4", "pass")));

        // dodajemy obiekty do listy (pojedyńczo)
        list.add(new User("jakis user", "dziwny password"));

        // wypisanie (wywola toString listy, ktory domyslnie iteruje
        // elementy listy i wypisuje je jeden obok drugiego -
        // wywołując na nich metodę toString())
        System.out.println(list);
    }

}
