package Zad34Autostrada;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Highway {
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("[HH:mm yyyy/MM/dd]");
    private Map<String, VehicleInfo> carRegistry = new HashMap<>();

    public Highway() {

    }

    public boolean vehicleEntry(String plates, CarType type) {
        if (carRegistry.containsKey(plates)) {
            System.out.println("Car already on highway");
            return false;
        }

        VehicleInfo newCarEntry = new VehicleInfo(plates, type);
        carRegistry.put(plates, newCarEntry);

        return true;
    }

    public VehicleInfo searchVehicle(String plates) {
        if (!carRegistry.containsKey(plates)) {
            System.out.println("Car is not on highway");
            return null;
        }
        System.out.println("Car is found on highway.");
        System.out.println("Car time of entry: " +
                carRegistry.get(plates).getEntryTime().format(formatter));

        return carRegistry.get(plates);
    }

    public Optional<Double> vehicleLeave(String plates) {
        if (!carRegistry.containsKey(plates)) {
            System.out.println("Car is not found on highway.");
            return Optional.empty();
        }

        VehicleInfo vehicleInfo = carRegistry.get(plates);
        carRegistry.remove(plates);

        long difference = Duration.between(vehicleInfo.getEntryTime(), LocalDateTime.now()).toMillis();
        double toPay = (difference / 1000.0) * vehicleInfo.getType().getPriceMultiplier();

        return Optional.of(new Double(toPay));
    }

    public Map<String, VehicleInfo> getCarRegistry() {
        return carRegistry;
    }

    public void setCarRegistry(Map<String, VehicleInfo> carRegistry) {
        this.carRegistry = carRegistry;
    }
}
