package Zad34Autostrada;
import java.util.Optional;
import java.util.Scanner;
/*
 Stwórz aplikację Autodstrada. Celem zadania będzie implementacja systemu autostrady.
 Do tej implementacji będzie nam potrzebny klasa Highway która reprezentuje autostradę, oraz klasa VehicleInfo
 która reprezentuje zbiór informacji o pojeździe - tam będziemy przechowywać takie dane jak jego rejestracja,
 jego typ(ciezarowka czy osobowy) i datę i godzinę wjazdu - po której będziemy rozliczać pojazdy wyjezdzające z autostrady.
    1. Stwórz enum CarType, który posiada typy TRUCK, CAR, MOTORCYCLE.
    2. Stwórz klasę VehicleInfo, która powinna posiadać numer rejestracyjny pojazdu (String), typ pojazdu (CarType),
    oraz datę WJAZDU na autostradę.
    3. Stwórz klasę Highway która będzie posiadać :
        - kolekcję wszystkich pojazdów
        - metodę vehicleEntry(String numer_rejestracyjny, oraz CarType type) - która dodaje do kolekcji nową instancję
        VehicleInfo oraz wypisuje do konsoli komunikat. Metoda nic nie zwraca.
        - metodę searchVehicle(String numer_rejestracyjny) - która szuka pojazdu i wypisuje jego informacje
        jeśli wciąż znajduje się na autostradzie
        - metodę wyjazdu - vehicleLeave(String numer_rejestracyjny) - która usuwa samochód z kolekcji, wypisuje komunikat,
    oraz na podstawie czasu jaki samochód znajdował się na autostradzie oblicza jej kwotę do zapłaty i wypisuje ją do konsoli.
    4. Stwórz main który przyjmuje komendy
    - wjazd NR_REJESTRACYJNY TYP
    - wyjazd NR_REJESTRACYJNY
    - sprawdz NR_REJESTRACYJNY
    i wykonuje odpowiednie akcje na instancji klasy highway.
    https://bitbucket.org/nordeagda2/highwaytohell
 */
public class MainAutostrada {
    public static void main(String[] args) {
        Highway highway = new Highway();

        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()) {
            String line = in.nextLine();
            String[] words = line.split(" ");

            try {
                if (words[0].toLowerCase().equals("enter")) {
                    String plates = words[1];
                    CarType carType = CarType.valueOf(words[2].toUpperCase());

                    if (highway.vehicleEntry(plates, carType)) {
                        System.out.println("Car entered into highway.");
                    } else {
                        System.out.println("Car didn't enter highway.");
                    }
                } else if (words[0].toLowerCase().equals("leave")) {
                    String plates = words[1];

                    Optional<Double> price = highway.vehicleLeave(plates);
                    if (price.isPresent()) {
                        System.out.println("Price for ride: " + price.get());
                    } else {
                        System.out.println("No price was returned");
                    }
                } else if (words[0].toLowerCase().equals("search")) {
                    String plates = words[1];

                    System.out.println(highway.searchVehicle(plates));
                }
            } catch (IllegalArgumentException iae) {
                System.out.println("Wrong car type was typed.");
            } catch (ArrayIndexOutOfBoundsException aioobe) {
                System.out.println("Wrong number of arguments. You should provide plates and car type.");
            }
        }
    }
}
