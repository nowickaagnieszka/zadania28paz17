package Zad20wyk2;

public interface Figure {
    double countArea();
    double countCircumference();
}
