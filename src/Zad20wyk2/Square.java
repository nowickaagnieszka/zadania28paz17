package Zad20wyk2;

public class Square implements Figure {
    private double a;

    public Square(double a) {
        this.a = a;
    }

    @Override
    public double countCircumference() {
        return 4*a;
    }

    @Override
    public double countArea() {
        return a*a;
    }
}

