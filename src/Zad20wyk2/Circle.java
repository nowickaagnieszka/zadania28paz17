package Zad20wyk2;

public class Circle implements Figure{
    private double a;

    public Circle(double a) {
        this.a =a ;
    }

    @Override
    public double countArea() {
        return 3.14 * (a*a);
    }

    @Override
    public double countCircumference() {
        return 2 * 3.14 * a;
    }
}
