package Zad20wyk2;

public class Triangle implements Figure {
    private double a, b, c;

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public double countArea() {
        return Math.sqrt((a+b+c)*(a+b-c)*(a-b+c)*(b-a+c))/4;
    }

    @Override
    public double countCircumference() {
        return a+b+c;
    }
}
