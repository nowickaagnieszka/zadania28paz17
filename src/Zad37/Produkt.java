package Zad37;
import javax.xml.crypto.Data;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class Produkt{
    private String nazwa;
    private double cena;
    private int ilosc;
    private Date dataWaznosci;

    public Produkt(String nazwa, double cena, int ilosc, Date dataWaznosci) {
        this.nazwa = nazwa;
        this.cena = cena;
        this.ilosc = ilosc;
        this.dataWaznosci = dataWaznosci;
    }

    public String getNazwa() {
        return nazwa;
    }

    public int getIlosc() {
        return ilosc;
    }

    public void setIlosc(int ilosc) {
        this.ilosc = ilosc;
    }
    public void setCena(double cena) {
        this.cena = cena;
    }

    public void aktualizuj(Produkt p1){
        this.cena = p1.cena;
        this.ilosc += p1.ilosc;
        this.dataWaznosci = p1.dataWaznosci;
    }
    public String wypisz(){

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dataWaznosci;

        String temp = nazwa+" "+cena+" "+ilosc+" "+ new SimpleDateFormat("dd.MM.yyyy").format(date);
        if (this.dataWaznosci.before(date)){
            temp += " - przeterminowane";
        }
        return temp;


    }
}


