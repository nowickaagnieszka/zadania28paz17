package Zad37;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

/*
Stosując zaimplementuj aplikację, która monitoruje terminy przydatności produktów.
Aplikacja powinna obsługiwać komendy. Dopuszczalne komendy to:
    - dodaj nazwa cena ilosc [termin przydatności]
    - usun nazwa
    - komenda dodaj: powinna do magazynu dodawać produkt o podanej nazwie.
Jeśli produkt o takiej nazwie istnieje to operacja dodaj powinna AKTUALIZOWAĆ CENĘ tego produktu oraz
    dodawać ilość produktu.
     Dodatkowe[trudniejsze]*: Pod każdą nazwą produktu powinna istnieć informacja ile mamy produktów starej ważności
    Podstawowe[łatwe]: Po dodaniu ilości produktów i aktualizacji ceny aktualizuj również termin przydatności.
    (tak, system jest głupi i zamiast posiadać stare produkty ze starą datą przydatności o nowe produkty z nową,
    to system posiada tylko sumę obu ilości produktów z tą datą przydatności którą wpisaliśmy ostatnio)

            czyli:
                --------------------------------------------------
                Podstawowe:
                    po komendzie
                            dodaj 2.50 30 23.10.2017-18:00

                    mam produkt:
                            masło 2.50 30 23.10.2017-18:00

                    po komendzie
                            dodaj masło 3.0 10 03.11.2017-19:00

                    mam produkt:
                            masło 3.00 40 03.11.2017-19:00
                --------------------------------------------------
                Dodatkowe:
                    po komendzie
                            dodaj 2.50 30 23.10.2017-18:00

                    mam produkt:
                            masło 2.50 30 23.10.2017-18:00

                    po komendzie
                            dodaj masło 3.0 10 03.11.2017-19:00

                    mam produkt:
                            masło 2.50 30 23.10.2017-18:00
                            masło 3.00 10 03.11.2017-19:00

- komenda usun: usuwa [wszystkie] produkty o podanej nazwie.
- komenda sprawdz: wypisuje wszystkie produkty w kolejności od tych ktore sa najblizej przeterminowania,
do tych ktore sa najdalej do przeterminowania.
 Przy kazdej pozycji przeterminowanej powinna sie dopisac wzmianka o tym ze sa przeterminowane.

                    Mając produkty:
                            masło 2.50 30 20.10.2017-18:00
                            masło 3.00 10 03.11.2017-19:00
                            chleb 3.00 5  10.10.2017-18:00
                            wódka 10.00 1 03.11.2017-19:00

                        i wiedząc że jest dzien 25.10.2017 powinineś wypisać:
                            masło:
                                    - 2.50 30 20.10.2017-18:00 - przeterminowane
                                    - 3.00 10 03.11.2017-19:00
                            chleb
                                    - 3.00 5  10.10.2017-18:00 - przeterminowane
                            wódka
                                    - 10.00 1 03.11.2017-19:00


                  Dodatkowe*: spróbuj użyć kolejki priorytetowej do przechowywania produktów
 */
public class Main37 {
    public static void main(String[] args) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String cmd;
        String[] cmdList;
        ArrayList<Produkt> listaProduktow = new ArrayList<Produkt>();
        Produkt temp;

        Scanner scr = new Scanner(System.in);
        boolean jest;

        while(true){
            try{
                jest = false;
                System.out.println("\nPodaj komendę: [dodaj/usun/wyswietl/wyjdz]");
                cmd = scr.nextLine();
                cmdList = cmd.split(" ");
                if(cmdList[0].equalsIgnoreCase("dodaj") && cmdList.length == 5){
                    temp = new Produkt(cmdList[1], Double.valueOf(cmdList[2]),Integer.valueOf(cmdList[3]),dateFormat.parse(cmdList[4]));
                    for(Produkt prod : listaProduktow){
                        if(prod.getNazwa().equalsIgnoreCase(cmdList[1])){
                            prod.aktualizuj(temp);
                            jest = true;
                            System.out.println("Produkt został zaktualizowany.");
                        }
                    }
                    if(!jest){
                        listaProduktow.add(temp);
                        System.out.println("Produkt został dodany do listy.");
                    }

                } else if(cmdList[0].equalsIgnoreCase("usun") && cmdList.length == 2){

                    for(int i = 0;i<listaProduktow.size();i++){
                        temp = listaProduktow.get(i);
                        if(temp.getNazwa().equalsIgnoreCase(cmdList[1])){
                            listaProduktow.remove(i);
                            jest=true;
                            System.out.println("Produkt został usunięty z listy.");
                        }
                    }
                    if(!jest){
                        System.out.println("Nie ma takiego produktu na liscie!");
                    }

                } else if(cmdList[0].equalsIgnoreCase("wyswietl") && cmdList.length == 1){
                    System.out.println("Lista produktów na liscie: " + listaProduktow.size());
                    for(Produkt prod : listaProduktow){
                        System.out.println(prod.wypisz());
                    }

                } else if(cmdList[0].equalsIgnoreCase("wyjdz") && cmdList.length == 1){

                    return;
                } else {
                    System.out.println("Zła komenda lub zła składnia komendy. Dostępne komendy:\n");
                    System.out.println("dodaj [nazwa] [cena] [ilość] [data przydatności]");
                    System.out.println("usun [nazwa]");
                    System.out.println("wyswietl");
                    System.out.println("wyjdz");
                }
            }catch (ParseException ex) {
                System.out.println("Error: "+ex.getMessage()+"\n Produkt nie został dodany do listy!\n Poprawny format daty to yyyy-MM-dd");
            }catch (NumberFormatException ex) {
                System.out.println("Error: "+ex.getMessage()+"\n Produkt nie został dodany do listy!\n Poprawny format ceny 0.00");
            }

        }


    }

}
