package Zad2;
/*
    Utwórz klasę TeddyBear, który będzie miał pole prywatne String name; Dodaj konstruktor, który przyjmie za parametr imię.
    Co to znaczy że pole jest prywatne?
    Dodaj metodę, która wyświetli imię misia;
 */
public class Main2 {
    public static void main(String[] args) {
        TeddyBear bear = new TeddyBear("Teddy");
        System.out.println("Imie misia to " + bear.getName());
    }
}
