package Zad2;

public class TeddyBear {
    private String name;  //jak mam private to musze dodac gettera by dostac sie pozniej do zmiennej

    public TeddyBear(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
