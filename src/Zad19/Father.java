package Zad19;

public class Father extends FamilyMember {

    public Father(String name, String surname, String whoIam) {

        super(name, surname, whoIam);
    }

    @Override
    public void introduce() {
        super.introduce();
        System.out.println("I am father: "+this.getName() +" "+ this.getSurname());
        System.out.println("I am just a simple family member: " + this.getWhoIam());
    }

    @Override
    public boolean isParent() {
        return true;
    }

    @Override
    public boolean isAdult() {
        return true;
    }

}
