package Zad19;


public class Daugther extends FamilyMember {

    public Daugther(String name, String surname, String whoIam) {
        super(name, surname, whoIam);
    }
    @Override
    public void introduce() {
        super.introduce();
        System.out.println("I am daughter: " + getName() +" " + getSurname());

    }
    @Override
    public boolean isParent(){
        return false;
    }

    @Override
    public boolean isAdult(){
        return false;
    }
}

