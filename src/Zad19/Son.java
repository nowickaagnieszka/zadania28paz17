package Zad19;

public class Son extends FamilyMember {
    public Son(String name, String surname, String whoIam) {
        super(name, surname, whoIam);
    }

    @Override
    public void introduce() {
        super.introduce();
        System.out.println("I am son: " + getName() +" " + getSurname());

    }

    public void playWithStick(){
        System.out.println("I am playing");
    }

    @Override
    public boolean isParent(){
        return false;
    }

    @Override
    public boolean isAdult(){
        return false;
    }

}
