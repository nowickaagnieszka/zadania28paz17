package Zad19;

public abstract class FamilyMember {
    protected String name;
    protected String surname;

    public String getWhoIam() {
        return whoIam;
    }

    public void setWhoIam(String whoIam) {
        this.whoIam = whoIam;
    }

    protected String whoIam;

    public FamilyMember(String name, String surname, String whoIam) {
        this.name = name;
        this.surname = surname;
        this.whoIam = whoIam;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void introduce(){
        System.out.println("Jestem w rodzinie.");
    }

    public abstract boolean isParent();

    public abstract boolean isAdult();

}
