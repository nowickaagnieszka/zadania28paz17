package Zad19;
/*
Utwórz klasę FamilyMember z polem name i metodą introduce() która wypisuje komunikat "I am just a simple family member".
    Wykorzystaj dziedziczenie (extends FamilyMember ) w klasach Mother, Father, Son, Daugther.
    W metodzie introduce poszczególnych klas wpisz implementację:
        - i am mother ...
        - i am father ...
        - i am son ...
        - i am daughter ...

    Umieść rodzinę w liście i za pomocą pętli foreach wywołaj metodę introduce().

    B)
    Dodaj metodę (*abstrakcyjną) boolean isAdult do klasy FamilyMember, załóż że rodzice są zawsze dorośli, a dzieci nie.
    Nadpisz te metody w klasach podrzędnych.
 */
public class Main19 {
    public static void main(String[] args) {
        FamilyMember[] fml = new FamilyMember[4];
        fml[0] = new Mother(" Aga", "Nazwisko", "matka");
        fml[1] = new Son("Olivier", "Nazwisko", "syn");
        fml[2] = new Daugther("Nina", "Nazwisko", "córka");
        fml[3] = new Father("Maciej", "Nazwisko", "ojciec");

        for (FamilyMember person:fml) {
            person.introduce();
        }

       }
}
