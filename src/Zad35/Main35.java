package Zad35;
import java.util.ArrayList;
import java.util.Date;

/*
Movie database:
Stwórz enum MovieType który posiada opcje:
    ACTION, DRAMA, COMEDY, HORROR
Stwórz model filmu, który : (klasa Movie)
    - posiada pole nazwa filmu - String
    - posiada pole typ filmu (enum)
    - posiada pole data wydania filmu - data
    - posiada pole nazwisko autora - String
1. Stwórz klasę MoviesDatabase która posiada:
    - jako pole posiada mapę filmów. Mapa powinna mapować z wartości typu String
    (będzie to nazwa filmu) na model (na obiekty klasy Movie)
    - stwórz metodę dodawania do bazy danych filmów ( addMovie(Movie m)) która dodaje do mapy film
    - stwórz metodę wyszukiwania filmów z bazy danych, która przyjmuje jako parametr nazwę filmu,
    a zwraca film który jest w bazie danych pod tą nazwą.
    - stwórz metodę wypisywania wszystkich filmów. Metoda powinna iterować po wartościach mapy i
    wypisywać informacje o filmie (nadpisz metodę toString w klasie ...[wiesz jakiej?]). metoda printAllMovies().
    - stwórz metodę o tej samej nazwie co powyższa, która przyjmuje jako parametr typ filmu (MovieType)
    i również iteruje wartości, jedak wypisuje tylko te filmy których MovieType odpowiada temu podanemu jako parametr.
2. Stwórz maina w którym będziesz testować tą funkcjonalność dodawania/wyszukiwania/wypisywania i filtrowania
(wypisywanie tylko tych z wybranej kategorii) filmów.
 */
public class Main35 {
    public static void main(String[] args) {
        MovieDatabase baza = new MovieDatabase();
        Movie m = new Movie("Titanic", new Date(2013, 03, 3),"Aga",MovieType.DRAMA);
        Movie m2 = new Movie("Titanic 2", new Date(2013, 03, 3),"Kasia",MovieType.COMEDY);

        baza.addMovie(m);
        baza.addMovie(m2);


        System.out.println(baza.findMovie("Titanic"));
        baza.printAllMovies(MovieType.COMEDY);
    }
}


