package Zad35;

import java.util.HashMap;
import java.util.Map;

public class MovieDatabase {
    private Map<String, Movie> map; // = new HashMap<String, Movie>();

    public MovieDatabase() {
        map = new HashMap<>();
    }

    public void addMovie(Movie film) {
        map.put(film.getName(), film);
    }

    public Movie findMovie(String tytul) {
        return map.get(tytul);
    }

    public void printAllMovies() {
        for (Movie film : map.values()) {
            System.out.println(film);
        }
    }

    public void printAllMovies(MovieType typSzukany) {
        for (Movie film : map.values()) {
            if (film.getType() == typSzukany) {
                System.out.println(film);
            }
        }
    }
}
