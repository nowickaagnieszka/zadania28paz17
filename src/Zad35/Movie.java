package Zad35;

import java.util.Date;

public class Movie {
        private String name;
        private Date rokWydania;
        private String author;
        private MovieType type;

        public Movie(String name, Date rokWydania, String author, MovieType type) {
            super();
            this.name = name;
            this.rokWydania = rokWydania;
            this.author = author;
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public MovieType getType() {
            return type;
        }

        public void setType(MovieType type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return "Movie [name=" + name + ", rokWydania=" + rokWydania + ", author=" + author + ", type=" + type + "]";
        }
}

