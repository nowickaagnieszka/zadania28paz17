package Zad33;
/*
Stwórz program a w nim enum'a Plec, a w nim dwie wartości: "Kobieta", "Mezczyzna"
Stwórz program który skanuje z wejścia linie, a następnie sprawdza czy wprowadzona linia zawiera wartości enuma:
(posługując się parsowaniem enuma rzucającego IllegalArgumentException)

Podpowiedz: String linia = scanner.nextLine();
            Plec wpisana = Plec.valueOf(linia.trim());
            System.out.println("Wpisano:" + wpisana);
 */
import java.util.Scanner;

public class Main33 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        boolean isWorking = true;
        do {
            String line = sc.nextLine();

            String upperTrimmed = line.toUpperCase().trim();
            switch (upperTrimmed) {
                case "QUIT":
                    isWorking = false;
                    break;
                default:
                    try {
                        Plec plec = Plec.valueOf(upperTrimmed);
                        System.out.println("Wpisano: " + plec);
                    } catch (IllegalArgumentException iae) {
                        System.out.println("Niepoprawna wartość: " + upperTrimmed);
                    }
            }
        } while (isWorking);
    }

    public static Plec changeToPlec(String slowo) {
        if (slowo.toUpperCase().trim().equals("KOBIETA")) {
            return Plec.KOBIETA;
        } else if (slowo.toUpperCase().trim().equals("MEZCZYZNA")) {
            return Plec.MEZCZYZNA;
        }
        // !! nie dodajemy specjalnie enuma
        return Plec.UNKNOWN; // niewiadoma plec - nie widoczne dla uzytk ma byc!
    }
}
